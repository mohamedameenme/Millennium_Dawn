
###Bergen
province = { id = 110 naval_base = { size = 4 current_size = 4 } }

###Stockholm
province = { id = 101 naval_base = { size = 4 current_size = 4 } }

###Copenhagen
province = { id = 294 naval_base = { size = 4 current_size = 4 } }

###Tallinn
province = { id = 194 naval_base = { size = 1 current_size = 1 } }

###Riga
province = { id = 197 naval_base = { size = 1 current_size = 1 } }

###Memel
province = { id = 206 naval_base = { size = 1 current_size = 1 } }

###Turku
province = { id = 141 naval_base = { size = 2 current_size = 2 } }

###Gdansk
province = { id = 303 naval_base = { size = 2 current_size = 2 } }

###Kiel
province = { id = 90 naval_base = { size = 10 current_size = 10 } }

###Rotterdam
province = { id = 46 naval_base = { size = 6 current_size = 6 } }

###Ghent
province = { id = 45 naval_base = { size = 4 current_size = 4 } }

###Brest
province = { id = 32 naval_base = { size = 10 current_size = 10 } }

###Marseille
province = { id = 364 naval_base = { size = 8 current_size = 8 } }

###Noumea
province = { id = 1692 naval_base = { size = 1 current_size = 1 } }

###Djibouti
province = { id = 1037 naval_base = { size = 2 current_size = 2 } }

###Barcelona
province = { id = 357 naval_base = { size = 8 current_size = 8 } }

###Oviedo
province = { id = 332 naval_base = { size = 4 current_size = 4 } }

###Oporto
province = { id = 335 naval_base = { size = 4 current_size = 4 } }

###Norwich
province = { id = 17 naval_base = { size = 8 current_size = 8 } }

###Plymouth
province = { id = 23 naval_base = { size = 10 current_size = 10 } }

###Diego Garcia Island
province = { id = 1817 naval_base = { size = 6 current_size = 6 } }

###Falkland Islands
province = { id = 866 naval_base = { size = 1 current_size = 1 } }

###Dublin
province = { id = 30 naval_base = { size = 2 current_size = 2 } }

###Reykjavik
province = { id = 1 naval_base = { size = 2 current_size = 2 } }

###Halifax
province = { id = 579 naval_base = { size = 8 current_size = 8 } }

###Vancuover
province = { id = 541 naval_base = { size = 6 current_size = 6 } }

###San Diego
province = { id = 761 naval_base = { size = 10 current_size = 10 } }

###Seattle
province = { id = 780 naval_base = { size = 5 current_size = 5 } }

###Guam
province = { id = 1624 naval_base = { size = 2 current_size = 2 } }

###Pearl Harbour
province = { id = 663 naval_base = { size = 8 current_size = 8 } }

###Norfolk
province = { id = 633 naval_base = { size = 10 current_size = 10 } }

###Boston
province = { id = 600 naval_base = { size = 5 current_size = 5 } }

###Jacksonville
province = { id = 648 naval_base = { size = 5 current_size = 5 } }

###Merida
province = { id = 756 naval_base = { size = 4 current_size = 4 } }

###Culiac�n
province = { id = 743 naval_base = { size = 4 current_size = 4 } }

###Colon
province = { id = 889 naval_base = { size = 2 current_size = 2 } }

###Havana
province = { id = 654 naval_base = { size = 2 current_size = 2 } }

###Antigua
province = { id = 897 naval_base = { size = 2 current_size = 2 } }

###Medellin
province = { id = 890 naval_base = { size = 4 current_size = 4 } }

###Caracas
province = { id = 811 naval_base = { size = 4 current_size = 4 } }

###Maraj�
province = { id = 877 naval_base = { size = 6 current_size = 6 } }

###Rio de Janeiro
province = { id = 885 naval_base = { size = 6 current_size = 6 } }

###Montevideo
province = { id = 849 naval_base = { size = 2 current_size = 2 } }

###Buenos Aires
province = { id = 667 naval_base = { size = 6 current_size = 6 } }

###Santiago
province = { id = 840 naval_base = { size = 4 current_size = 4 } }

###Lima
province = { id = 830 naval_base = { size = 4 current_size = 4 } }

###Quito
province = { id = 815 naval_base = { size = 2 current_size = 2 } }

###Cape Town
province = { id = 1116 naval_base = { size = 4 current_size = 4 } }

###Beira
province = { id = 1130 naval_base = { size = 4 current_size = 4 } }

###Banana
province = { id = 1095 naval_base = { size = 4 current_size = 4 } }

###Douala
province = { id = 1089 naval_base = { size = 2 current_size = 2 } }

###Port �tienne
province = { id = 973 naval_base = { size = 4 current_size = 4 } }

###Casablanca
province = { id = 958 naval_base = { size = 4 current_size = 4 } }

###Algiers
province = { id = 949 naval_base = { size = 4 current_size = 4 } }

###Tunis
province = { id = 939 naval_base = { size = 4 current_size = 4 } }

###Tripoli
province = { id = 932 naval_base = { size = 4 current_size = 4 } }

###Alexandria
province = { id = 906 naval_base = { size = 4 current_size = 4 } }

###Port Sudan
province = { id = 1030 naval_base = { size = 2 current_size = 2 } }

###Haifa
province = { id = 404 naval_base = { size = 4 current_size = 4 } }

###Beirut
province = { id = 1794 naval_base = { size = 2 current_size = 2 } }

###Aleppo
province = { id = 1862 naval_base = { size = 4 current_size = 4 } }

###Cyprus
province = { id = 444 naval_base = { size = 1 current_size = 1 } }

###Izmir
province = { id = 437 naval_base = { size = 8 current_size = 8 } }

###Athens
province = { id = 401 naval_base = { size = 8 current_size = 8 } }

###Tirana
province = { id = 390 naval_base = { size = 2 current_size = 2 } }

###Podgorica
province = { id = 389 naval_base = { size = 2 current_size = 2 } }

###Split
province = { id = 384 naval_base = { size = 2 current_size = 2 } }

###Mostar
province = { id = 387 naval_base = { size = 2 current_size = 2 } }

###Taranto
province = { id = 523 naval_base = { size = 10 current_size = 10 } }

###Malta
province = { id = 31 naval_base = { size = 1 current_size = 1 } }

###Varna
province = { id = 422 naval_base = { size = 4 current_size = 4 } }

###Tulcea
province = { id = 436 naval_base = { size = 4 current_size = 4 } }

###Chisinev
province = { id = 434 naval_base = { size = 1 current_size = 1 } }

###Sevastopol
province = { id = 253 naval_base = { size = 6 current_size = 6 } }

###Rostov
province = { id = 258 naval_base = { size = 4 current_size = 4 } }

###Kalingrad
province = { id = 510 naval_base = { size = 10 current_size = 10 } }

###St. Petersburg
province = { id = 187 naval_base = { size = 4 current_size = 4 } }

###Murmansk
province = { id = 133 naval_base = { size = 10 current_size = 10 } }

###Vladivostok
province = { id = 1372 naval_base = { size = 8 current_size = 8 } }

###Tokyo
province = { id = 1184 naval_base = { size = 8 current_size = 8 } }

###Fukuoka
province = { id = 1190 naval_base = { size = 6 current_size = 6 } }

###Busan
province = { id = 1196 naval_base = { size = 6 current_size = 6 } }

###Hamhung
province = { id = 1392 naval_base = { size = 6 current_size = 6 } }

###Ningbo
province = { id = 1239 naval_base = { size = 10 current_size = 10 } }

###Hainan
province = { id = 1321 naval_base = { size = 10 current_size = 10 } }

###Yantai
province = { id = 1214 naval_base = { size = 5 current_size = 5 } }

###Taipei
province = { id = 1323 naval_base = { size = 6 current_size = 6 } }

###Manila
province = { id = 1737 naval_base = { size = 4 current_size = 4 } }

###Nha Trang
province = { id = 1335 naval_base = { size = 4 current_size = 4 } }

###Battambang
province = { id = 1339 naval_base = { size = 1 current_size = 1 } }

###Bangkok
province = { id = 1343 naval_base = { size = 6 current_size = 6 } }

###Kuantan
province = { id = 1351 naval_base = { size = 4 current_size = 4 } }

###Singapore
province = { id = 1353 naval_base = { size = 4 current_size = 4 } }

###Batavia
province = { id = 1628 naval_base = { size = 6 current_size = 6 } }

###Kokonav
province = { id = 1662 naval_base = { size = 4 current_size = 4 } }

###Rangoon
province = { id = 1299 naval_base = { size = 2 current_size = 2 } }

###Chittagong
province = { id = 1288 naval_base = { size = 4 current_size = 4 } }

###Calcutta
province = { id = 1457 naval_base = { size = 8 current_size = 8 } }

###Madurai
province = { id = 1515 naval_base = { size = 8 current_size = 8 } }

###Rajkot
province = { id = 1465 naval_base = { size = 6 current_size = 6 } }

###Karachi
province = { id = 1494 naval_base = { size = 6 current_size = 6 } }

###Bandar Abbas
province = { id = 1497 naval_base = { size = 6 current_size = 6 } }

###Basrah
province = { id = 1823 naval_base = { size = 4 current_size = 4 } }

###Kuweit City
province = { id = 1822 naval_base = { size = 4 current_size = 4 } }

###Dammam
province = { id = 1821 naval_base = { size = 4 current_size = 4 } }

###Jiddah
province = { id = 1808 naval_base = { size = 2 current_size = 2 } }

###Doha
province = { id = 1820 naval_base = { size = 2 current_size = 2 } }

###Dubai
province = { id = 1504 naval_base = { size = 4 current_size = 4 } }

###Mascate
province = { id = 1819 naval_base = { size = 2 current_size = 2 } }

###Aden
province = { id = 1813 naval_base = { size = 2 current_size = 2 } }

###Tananarive
province = { id = 1137 naval_base = { size = 1 current_size = 1 } }

###East Timor
province = { id = 1658 naval_base = { size = 1 current_size = 1 } }

###Sydney
province = { id = 1727 naval_base = { size = 6 current_size = 6 } }

###Darwin
province = { id = 1703 naval_base = { size = 4 current_size = 4 } }

###Perth
province = { id = 1712 naval_base = { size = 4 current_size = 4 } }

###Fiji
province = { id = 1752 naval_base = { size = 2 current_size = 2 } }

###Port Moresby
province = { id = 1679 naval_base = { size = 2 current_size = 2 } }

###Majuro
province = { id = 1605 naval_base = { size = 2 current_size = 2 } }

###Wellington
province = { id = 1871 naval_base = { size = 4 current_size = 4 } }

###Lagos
province = { id = 1008 naval_base = { size = 2 current_size = 2 } }