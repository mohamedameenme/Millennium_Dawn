#Gets log with Author Name (%an) and subject (%s) and saves to a file named full_log.txt
git log --pretty=format:"%an - %s" --no-merges >> full_log.txt

#gets log with Date relative (%ar), Author Name (%an) and subject (%s) and saves to a file named full_log.txt
git log --pretty=format:"%ar: %an - %s" --no-merges >> full_log.txt

##Gets log with Author Name (%an) and subject (%s) within the last 2 weeks and saves to a file named temp_log.txt
git log --pretty=format:"%an - %s" --since=2.weeks --no-merges >> temp_log.txt