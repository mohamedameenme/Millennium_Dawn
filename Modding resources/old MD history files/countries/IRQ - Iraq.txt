﻿capital = 291

oob = "IRQ_2000"

set_convoys = 100
set_stability = 0.5

set_country_flag = country_language_arabic
set_country_flag = country_language_kurdish

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	corvette1 = 1
	corvette2 = 2
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

add_ideas = {
	population_growth_explosion
	arab_league_member
	extensive_conscription
}

set_politics = {

	parties = {
		islamist = {
			popularity = 20
		}
		nationalist = {
			popularity = 42
		}
		reactionary = {
			popularity = 18
		}
		conservative = {
			popularity = 10
		}
		social_democrat = {
			popularity = 2
		}
		communist = {
			popularity = 8
		}
	}
	
	ruling_party = nationalist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Saddam Hussein"
	ideology = autocrat
	picture = "Saddam_Hussein.dds"
	expire = "2004.1.1"
	
	traits = {}
}
create_country_leader = {
	name = "Ra'ad I"
	ideology = absolute_monarchist
	picture = "Raad.dds"
}

create_country_leader = {
	name = "Muqtada al-Sadr"
	ideology = counter_progressive_democrat
	picture = "Muqtada_al_Sadr.dds"
}

create_country_leader = {
	name = "Ibrahim al-Jaafari"
	ideology = right_wing_conservative
	picture = "Ibrahim_al_Jaafari.dds"
}

create_country_leader = {
	name = "Adnan Pachachi"
	ideology = libertarian
	picture = "Adnan_Pachachi.dds"
}

create_country_leader = {
	name = "Ahmed Chalabi"
	ideology = centrist
	picture = "Ahmed_Chalabi.dds"
}

create_country_leader = {
	name = "Naseer al-Chaderchi"
	ideology = social_democrat_ideology
	picture = "Naseer_Chaderchi"
}

create_country_leader = {
	name = "Ayad Allawi"
	ideology = progressive_ideology
	picture = "Ayad_Allawi.dds"
}

create_country_leader = {
	name = "Hamid Majid Mousa"
	ideology = marxist
	picture = "Hamid_Majid_Mousa.dds"
}

create_country_leader = {
	name = "Mohammad Baqir al-Hakim"
	ideology = islamic_authoritarian
	picture = "Mohammad_Baqir_al_Hakim.dds"
}

create_country_leader = {
	name = "Yonadam Kanna"
	picture = "Yonadam_Kanna.dds"
	ideology = national_socialist
}

2004.1.1 = {
	set_politics = {
		parties = {
			islamist = {
				popularity = 30
			}
			nationalist = {
				popularity = 15
			}
			reactionary = {
				popularity = 18
			}
			conservative = {
				popularity = 30
			}
			market_liberal = {
				popularity = 1
			}
			social_democrat = {
				popularity = 2
			}
			communist = {
				popularity = 8
			}
		}
		ruling_party = conservative
		elections_allowed = yes
	}

}

2003.8.30 = {	#Saddam is toppled
	add_stability = -0.1
    create_country_leader = {
        name = "Izzat Ibrahim al-Douri"
		ideology = autocrat
	    picture = "Portrait_Izzat_Ibrahim_al-Douri.dds"
    }
	create_country_leader = {
	    name = "Abdul Aziz al-Hakim"
	    ideology = islamic_authoritarian
	    picture = "Abdul_Aziz_al_Hakim.dds"
    }
	add_ideas = {
		islamic_sectarian_conflicts
	}
}

2006.5.20 = {
create_country_leader = {
	    name = "Nouri al-Maliki"
	    picture = "Nouri_Maliki.dds"
	    ideology = fiscal_conservative
    }
}

2009.1.1 = {
    create_country_leader = {
	    name = "Ammar al-Hakim"
	    ideology = islamic_authoritarian
	    picture = "Ammar_al_Hakim.dds"
    }
}

2016.5.1 = { oob = "IRQ_2016" }

2014.9.8 = {
    create_country_leader = {     #President Fuad Masum nominatines Haidar al-Abadi to replace Al-Maliki for constitutional violation
	    name = "Haider al-Abadi"
	    picture = "Haider_Abadi.dds"
	    ideology = fiscal_conservative
    }
}