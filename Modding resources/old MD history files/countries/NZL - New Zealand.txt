﻿capital = 284

oob = "NZL_2000"

set_convoys = 210
set_stability = 0.5

set_country_flag = country_language_english

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1

	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_steady
	commonwealth_of_nations_member
}

set_politics = {

	parties = {
		reactionary = {
			popularity = 4
		}
		conservative = {
			popularity = 37.11
		}
		market_liberal = {
			popularity = 7
		}
		social_liberal = {
			popularity = 0.5
		}
		social_democrat = {
			popularity = 45.14
		}
		progressive = {
			popularity = 5
        }
		democratic_socialist = {
			popularity = 0.25
        }
		nationalist = {
			popularity = 1
		}
	}
	
	ruling_party = conservative
	last_election = "1999.11.27"
	election_frequency = 36
	elections_allowed = yes
}

add_opinion_modifier = {
	target = AST
	modifier = ANZUS
}

add_opinion_modifier = {
	target = AST
	modifier = ANZUS_trade
}

add_opinion_modifier = {
	target = AST
	modifier = five_eyes_agreement
}

add_opinion_modifier = {
	target = CAN
	modifier = five_eyes_agreement
}

add_opinion_modifier = {
	target = ENG
	modifier = five_eyes_agreement
}

add_opinion_modifier = {
	target = USA
	modifier = ANZUS
}

add_opinion_modifier = {
	target = USA
	modifier = ANZUS_trade
}

add_opinion_modifier = {
	target = USA
	modifier = five_eyes_agreement
}

create_country_leader = {
	name = "Anwar-ul Ghani"
	picture = "Anwar_ul_Ghani.dds"
	ideology = islamic_republican
}

create_country_leader = {
	name = "Kyle Chapman"
	picture = "Kyle_Chapman.dds"
	ideology = national_socialist
}

create_country_leader = {
	name = "Anton Foljambe"
	picture = "Anton_Foljambe.dds"
	ideology = national_democrat
}

create_country_leader = {
	name = "Anne I of Windsor"
	picture = "Anne.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Winston Peters"
	picture = "Winston_Peters.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Jennifer Shipley"
	picture = "Jennifer_Shipley.dds"
	ideology = fiscal_conservative
}

create_country_leader = {
	name = "Richard Prebble"
	picture = "Richard_Prebble.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Rod Donald"
	picture = "Rod_Donald.dds"
	ideology = green
}

create_country_leader = {
	name = "Peter Dunne"
	picture = "Peter_Dunne.dds"
	ideology = centrist
}

create_country_leader = {
	name = "Helen Clark"
	picture = "Helen_Clark.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Angeline Greensill"
	picture = "Angeline_Greensill.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Annalucia Vermunt"
	picture = "Annalucia_Vermunt.dds"
	ideology = leninist
}

add_namespace = {
	name = "nzl_unit_leader"
	type = unit_leader
}

create_field_marshal = {
	name = "Kevin Short"
	picture = "generals/Kevin_Short.dds"
	traits = { logistics_wizard }
	skill = 1
}

create_field_marshal = {
	name = "Clive Douglas"
	picture = "generals/Clive_Douglas.dds"
	traits = {  }
	skill = 1
}

create_field_marshal = {
	name = "Michael Shapland"
	picture = "generals/Michael_Shapland.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Richard Rhys Jones"
	picture = "generals/Richard_R_Jones.dds"
	skill = 2
}

create_corps_commander = {
	name = "Peter Kelly"
	picture = "generals/Peter_Kelly.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Christopher Parsons"
	picture = "generals/Christopher_Parsons.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Christopher Faulls"
	picture = "generals/Christopher_Faulls.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Tony Davies"
	picture = "generals/Tony_Davies.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Tim Gall"
	picture = "generals/Tim_Gall.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Danny Broughton"
	picture = "generals/Danny_Broughton.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "John Martin"
	picture = "admirals/John_Martin.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 1
}

create_navy_leader = {
	name = "Wayne Dyke"
	picture = "admirals/Wayne_Dyke.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "David Gibbs"
	picture = "admirals/David_Gibbs.dds"
	traits = { blockade_runner }
	skill = 1
}

create_navy_leader = {
	name = "Jim Gilmour"
	picture = "admirals/Jim_Gilmour.dds"
	traits = { spotter }
	skill = 1
}

2001.10.8 = {
	create_country_leader = {
		name = "Bill English"
		picture = "Bill_English.dds"
		ideology = libertarian
	}
}
2003.10.28 = {
	create_country_leader = {
		name = "Don Brash"
		picture = "Don_Brash.dds"
		ideology = libertarian
	}
}
2008.11.19 = {
	create_country_leader = {
		name = "Phil Goff"
		picture = "Phil_Goff.dds"
		ideology = social_democrat_ideology
	}
}
2011.4.30 = { 
    set_party_name = {
		ideology = democratic_socialist
		long_name = NZL_democratic_socialist_ideology_party_Mana_Party_long
		name = NZL_democratic_socialist_ideology_party_Mana_Movement
    }
	create_country_leader = {
		name = "Hone Harawira"
		picture = "Hone_Harawira.dds"
		ideology = democratic_socialist_ideology
	}
}
2011.12.13 = {
    create_country_leader = {
		name = "David Shearer"
		picture = "David_Shearer.dds"
		ideology = social_democrat_ideology
	}
}
2013.9.15 = {
    create_country_leader = {
		name = "David Cunliffe"
		picture = "David_Cunliffe.dds"
		ideology = social_democrat_ideology
	}
}
2014.2.1 = {
    create_field_marshal = {
		name = "Tim Keating"
		picture = "generals/Tim_Keating.dds"
		skill = 1
	}
}
2016.6.1 = {
	set_politics = {
        last_election = "2014.9.20"
		ruling_party = conservative
		elections_allowed = yes
		parties = {
			social_liberal = {
				popularity = 1
			}
			conservative = {
				popularity = 52
			}
			market_liberal = {
				popularity = 1
			}
			progressive = {
				popularity = 11
			}
			reactionary = {
				popularity = 6
            }
            social_democrat = {
				popularity = 28
			}
			democratic_socialist = {
				popularity = 1
			}
		}
	}
	create_country_leader = {
		name = "John Key"
		picture = "John_Key.dds"
		ideology = fiscal_conservative
	}
	create_country_leader = {
		name = "Andrew Little"
		picture = "Andrew_Little.dds"
		ideology = social_democrat_ideology
	}
	create_country_leader = {
		name = "Metiria Turei"
		picture = "Metiria_Turei.dds"
		ideology = green
	}
	create_country_leader = {
		name = "David Seymour"
		picture = "David_Seymour.dds"
		ideology = libertarian
	}
	create_country_leader = {
		name = "Hazim Arafeh"
		picture = "Hazim_Arafeh.dds"
		ideology = islamic_republican
	}
}