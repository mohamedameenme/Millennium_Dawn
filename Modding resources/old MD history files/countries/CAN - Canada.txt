﻿capital = 276

oob = "CAN_2000"

set_research_slots = 5
set_stability = 0.5

set_country_flag = country_language_english
set_country_flag = country_language_french

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	corvette3 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	missile_destroyer_2 = 1
	missile_destroyer_3 = 1


	modern_carrier_0 = 1
	modern_carrier_1 = 1
	modern_carrier_2 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_stagnation
	quebec_separatism
	commonwealth_of_nations_member
}

set_politics = {
	parties = {
		conservative = { popularity = 32 }
		market_liberal = { popularity = 8 }
		social_liberal = { popularity = 38 }
		social_democrat = { popularity = 13 }
		progressive = { popularity = 2 }
		democratic_socialist = { popularity = 7 }
	}
	ruling_party = social_liberal
	last_election = "1997.6.2"
	election_frequency = 48
	elections_allowed = yes
}

add_opinion_modifier = {
	target = AST
	modifier = five_eyes_agreement
}

add_opinion_modifier = {
	target = ENG
	modifier = five_eyes_agreement
}

add_opinion_modifier = {
	target = NZL
	modifier = five_eyes_agreement
}

add_opinion_modifier = {
	target = USA
	modifier = american_canadian_friendship
}

add_opinion_modifier = {
	target = USA
	modifier = five_eyes_agreement
}

add_opinion_modifier = {
	target = USA
	modifier = NAFTA
}

add_opinion_modifier = {
	target = MEX
	modifier = NAFTA
}

create_country_leader = {
	name = "John McGuire"
	picture = "John_McGuire.dds"
	ideology = islamic_authoritarian
}

create_country_leader = {
	name = "Andrew I of Windsor"
	picture = "Andrew.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Stockwell Day" #Conservative
	picture = "Stockwell_Day.dds"
	ideology = fiscal_conservative
}

create_country_leader = {
	name = "Jean Chretien" #Liberal
	picture = "Jean_Chretien.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Joan Russow" #Green
	picture = "Joan_Russow.dds"
	ideology = green
}

create_country_leader = {
	name = "Alexa McDonough" #NPD
	picture = "Alexa_McDonough.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Barry Weisleder" 
	picture = "Barry_Weisleder.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Ricardo Duchesne" 
	picture = "Ricardo_Duchesne.dds"
	ideology = national_democrat
}

create_country_leader = {
	name = "Don Andrews" 
	picture = "Don_Andrews.dds"
	ideology = national_socialist
}

create_country_leader = {
	name = "Miguel Figueroa" 
	picture = "Miguel_Figueroa.dds"
	ideology = marxist
}

create_corps_commander = {
	name = "J. C. G. Juneau"
	picture = "generals/JCG_Juneau.dds"
	skill = 2
}

create_corps_commander = {
	name = "Jean-Marc Lanthier"
	picture = "generals/Jean_Marc_Lanthier.dds"
	skill = 1
}

create_corps_commander = {
	name = "Rob Roy MacKenzie"
	picture = "generals/Rob_Roy_MacKenzie.dds"
	skill = 1
}

create_corps_commander = {
	name = "Paul Wynnyk"
	picture = "generals/Paul_Wynnyk.dds"
	skill = 1
}

2003.1.1 = {
	set_party_name = {
		ideology = conservative
		long_name = CAN_conservative_party_con_long
		name = CAN_conservative_party_con
	}
}

2015.11.1 = {
	set_politics = {
		parties = {
			fascist = { popularity = 0.1 }
			nationalist = { popularity = 0.1 }
			social_liberal = { popularity = 39.7 }
			conservative = { popularity = 33.3 }
			market_liberal = { popularity = 1 }
			social_democrat = { popularity = 20.7 }
			progressive = { popularity = 4.8 }
			communist = { popularity = 0.3 }
		}
		last_election = "2015.10.19"
		elections_allowed = yes
	}
}

2016.1.1 = {
	create_country_leader = {
		name = "Justin Trudeau"
		picture = "Justin_Trudeau.dds"
		ideology = moderate
	}
	create_country_leader = {
		name = "Stephen Harper"
		picture = "Stephen_Harper.dds"
		ideology = fiscal_conservative
	}
	create_country_leader = {
		name = "Elizabeth May"
		picture = "Elizabeth_May.dds"
		ideology = green
	}
	create_country_leader = {
		name = "Thomas Mulcair"
		picture = "Thomas_Mulcair.dds"
		ideology = social_democrat_ideology
	}
}

2017.1.1 = {

oob = "CAN_2017"

set_variable = { var = debt value = 869 }
set_variable = { var = int_investments value = 13.4 }
set_variable = { var = treasury value = 83 }
set_variable = { var = tax_rate value = 40 }

add_ideas = {
	pop_050
	negligible_corruption
	pluralist
	gdp_9
	g7_member
		stable_growth
		defence_01
	export_economy
	edu_04
	health_05
	social_04
	bureau_03
	police_01
	volunteer_army
	volunteer_women
	intervention_limited_interventionism
	NATO_member
	western_country
	medium_far_right_movement
	small_medium_business_owners
	landowners
	fossil_fuel_industry
	common_law
}
set_country_flag = gdp_9
set_country_flag = TPP_Signatory
set_country_flag = Major_Importer_US_Arms
set_country_flag = positive_small_medium_business_owners
set_country_flag = positive_landowners
set_country_flag = enthusiastic_fossil_fuel_industry       


# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1

	#Colt C8
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	
	combat_eng_equipment = 1
	
	night_vision_1 = 1
	night_vision_2 = 1
	night_vision_3 = 1
	
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	command_control_equipment4 = 1
	
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	land_Drone_equipment2 = 1
	land_Drone_equipment3 = 1
	
	Early_APC = 1
	
	APC_1 = 1
	APC_2 = 1
	APC_3 = 1
	APC_4 = 1
	APC_5 = 1
	
	IFV_1 = 1
	IFV_2 = 1
	IFV_3 = 1
	IFV_4 = 1
	
	Rec_tank_0 = 1
	Rec_tank_1 = 1
	Rec_tank_2 = 1
	
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	util_vehicle_equipment_2 = 1
	util_vehicle_equipment_3 = 1
	util_vehicle_equipment_4 = 1
	util_vehicle_equipment_5 = 1
	
	MBT_1 = 1
	ENG_MBT_1 = 1
	
	gw_artillery = 1
	Arty_upgrade_1 = 1
	
	Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Anti_tank_1 = 1
	Heavy_Anti_tank_0 = 1
	
	Anti_Air_0 = 1
	
	early_bomber = 1
	naval_plane1 = 1
	naval_plane2 = 1
	
	#Halifax Class
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	
	#Iroquois Class
	destroyer_1 = 1
	destroyer_2 = 1
	
	#Victoria Class
	diesel_attack_submarine_1 = 1
	diesel_attack_submarine_2 = 1
	diesel_attack_submarine_3 = 1
	
	landing_craft = 1

}

#NATO military access
diplomatic_relation = {
	country = ALB
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = BEL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = BUL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CRO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = CZE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = DEN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = EST
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = FRA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GER
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = GRE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HUN
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ICE
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ITA
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LAT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LIT
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = LUX
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = HOL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = NOR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POL
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = POR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ROM
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLO
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SLV
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = SPR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = TUR
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = ENG
	relation = military_access
	active = yes
}
diplomatic_relation = {
	country = USA
	relation = military_access
	active = yes
}

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_30K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot
	complete_national_focus = Generic_30_IC_slot

set_politics = {

	parties = {
		democratic = { 
			popularity = 90
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 0
			#banned = no #default is no
		}
		
		neutrality = {
			popularity = 8
		}
		
		nationalist = {
			popularity = 2
		}
	}
	
	ruling_party = democratic
	last_election = "2015.10.19"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Justin Trudeau"
	desc = "POLITICS_MACKENZIE_KING_DESC"
	picture = "CAN_Justin_Trudeau.dds"
	expire = "2019.10.21"
	ideology = liberalism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Elizabeth May"
	desc = "POLITICS_MACKENZIE_KING_DESC"
	picture = "CAN_Elizabeth_May.dds"
	expire = "2023.10.23"
	ideology = Neutral_green
	traits = {
		#
	}
}
create_country_leader = {
	name = "Elizabeth Rowley"
	desc = "POLITICS_MACKENZIE_KING_DESC"
	picture = "CAN_Elizabeth_Rowley.dds"
	expire = "2023.10.23"
	ideology = Neutral_Communism
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Paul Wynnyk"
	picture = ""
	#traits = { thorough_planner }
	skill = 5
}
create_field_marshal = {
	name = "Alain Guimond"
	picture = ""
	#traits = { logistics_wizard charismatic }
	skill = 3
}
create_corps_commander = {
	name = "Carl Turenne"
	picture = ""
	#traits = { hill_fighter trait_engineer winter_specialist }
	skill = 2
}
create_corps_commander = {
	name = "Hercule Gosselin"
	picture = ""
	#traits = { trait_mountaineer winter_specialist }
	skill = 2
}
create_corps_commander = {
	name = "S.C. Hetherington"
	picture = ""
	#traits = { trait_mountaineer winter_specialist ranger }
	skill = 2
}
create_corps_commander = {
	name = "S.M. Cadden"
	picture = ""
	#traits = { trait_mountaineer winter_specialist ranger }
	skill = 3
}
create_navy_leader = {
	name = "Ron Lloyd"
	picture = ""
	#traits = { superior_tactician sea_wolf }
	skill = 5
}
create_navy_leader = {
	name = "Peter Bissonnette"
	picture = ""
	#traits = { }
	skill = 3
}
create_navy_leader = {
	name = "Fred George"
	picture = ""
	#traits = { }
	skill = 3
}
create_field_marshal = {
	name = "Jonathan Vance"
	picture = "Portrait_Jonathan_Vance.dds"
	traits = { old_guard inspirational_leader }
	skill = 4
}


create_corps_commander = {
	name = "Dean Milner"
	picture = "Portrait_Dean_Milner.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Christian Juneau"
	picture = "Portrait_Christian_Juneau.dds"
	traits = { trickster }
	skill = 2
}

create_corps_commander = {
	name = "Omer Lavoie"
	picture = "Portrait_Omer_Lavoie.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "C.J. Turenne"
	picture = "Portrait_C_J_Turenne.dds"
	traits = { winter_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Eric Landry"
	picture = "Portrait_Eric_Landry.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Stéphan Joudry"
	picture = "Portrait_Stephan_Joudrey.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "John Cochrane"
	picture = "Portrait_John_Cochrane.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "John Hlibchuk"
	picture = "Portrait_Hlibchuk.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Sylvie Pelletier"
	picture = "Portrait_Sylvie_Pelletier.dds"
	traits = { bearer_of_artillery }
	skill = 1
}

create_corps_commander = {
	name = "Wajahat Ali Beg"
	picture = "Portrait_Wajahat_Ali_Beg.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Jonathan Chouinard"
	picture = "Portrait_Jonathan_Chouinard.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Bill Fletcher"
	picture = "Portrait_Bill_Fletcher.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Luc Girouard"
	picture = "Portrait_Luc_Girouard.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Geoff Abthorpe"
	picture = "Portrait_Geoff_Abthorpe.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Eppo van Weelderen"
	picture = "Portrait_Eppo_van_Weelderen.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Josée Robidoux"
	picture = "Portrait_Josee_Robidoux.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Stephen Bowes"
	picture = "Portrait_Stephen_Bowes.dds"
	traits = { trait_engineer }
	skill = 3
}

create_corps_commander = {
	name = "Michael Hood"
	picture = "Portrait_Michael_Hood.dds"
	traits = {  }
	skill = 3
}

create_corps_commander = {
	name = "Michel Rouleau"
	picture = "Portrait_Michel_Rouleau.dds"
	traits = { naval_invader }
	skill = 2
}

create_corps_commander = {
	name = "Denis Thompson"
	picture = "Portrait_Denis_Thompson.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Stephen Hunter"
	picture = "Portrait_Steven_Hunter.dds"
	traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = {
	name = "Blaise Cathcart"
	picture = "Portrait_Blaise_Cathcart.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Colleen Halpin"
	picture = "Portrait_Colleen_Halpin.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Alan Guimond"
	picture = "Portrait_Alan_Guimond.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Gérard Poitras"
	picture = "Portrait_Gerard_Poitras.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Charles Lamarre"
	picture = "Portrait_C_A_Lamarre.dds"
	traits = { trait_engineer }
	skill = 3
}

create_corps_commander = {
	name = "H.C. MacKay"
	picture = "Portrait_H_C_MacKay.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Jean-Robert Bernier"
	picture = "Portrait_Jean_Robert_Bernier.dds"
	traits = {  }
	skill = 2
}

create_corps_commander = {
	name = "Pierre St-Amand"
	picture = "Portrait_Pierre_St-Amand.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Terry Garand"
	picture = "Portrait_Terry_Garand.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Stuart Hartnell"
	picture = "Portrait_Stuart_Hartnell.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Jennie Carignan"
	picture = "Portrait_Jennie_Carignan.dds"
	traits = { trait_engineer }
	skill = 1
}

create_corps_commander = {
	name = "Derek Macaulay"
	picture = "Portrait_Derek_Macauley.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Rob Roy MacKenzie"
	picture = "Portrait_Rob_Roy_MacKenzie.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Jean-Marc Lanthier"
	picture = "Portrait_Jean-Marc_Lanthier.dds"
	traits = { trickster }
	skill = 2
}

create_navy_leader = {
	name = "Bill Truelove"
	picture = "Portrait_Bill_Truelove.dds"
	traits = { seawolf }
	skill = 2
}

create_navy_leader = {
	name = "Art McDonald"
	picture = "Portrait_Art_MacDonald.dds"
	traits = { blockade_runner }
	skill = 2
}

create_navy_leader = {
	name = "Jeff Zwick"
	picture = "Portrait_Jeff_Zwick.dds"
	traits = { spotter }
	skill = 1
}

create_navy_leader = {
	name = "Michel Vigneault"
	picture = "Portrait_Michel_Vigneault.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Marta Mulkins"
	picture = "Portrait_M_B_Mulkins.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "John Newton"
	picture = "Portrait_John_Newton.dds"
	traits = { ironside }
	skill = 2
}

create_navy_leader = {
	name = "Craig Baines"
	picture = "Portrait_Craig_Baines.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Scott Bishop"
	picture = "Portrait_Scott_Bishop.dds"
	traits = { ironside }
	skill = 2
}

create_navy_leader = {
	name = "Gilles Couturier"
	picture = "Portrait_Gilles_Couturier.dds"
	traits = { blockade_runner }
	skill = 2
}

create_navy_leader = {
	name = "David Arsenault"
	picture = "Portrait_David_Arsenault.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Gilles Grégoire"
	picture = "Portrait_Gilles_Gregoire.dds"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Steven Waddell"
	picture = "Portrait_Steven_Waddell.dds"
	traits = {  }
	skill = 1
}
}