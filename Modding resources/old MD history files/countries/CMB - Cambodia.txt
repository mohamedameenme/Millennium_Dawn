﻿capital = 286

oob = "CMB_2000"

set_convoys = 10
set_stability = 0.5

set_country_flag = country_language_khmer

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_steady
}

set_politics = {

	parties = {
		
		islamist = {
			popularity = 0
		}
		nationalist = {
			popularity = 0
		}
		reactionary = {
			popularity = 30
		}
		conservative = {
			popularity = 40
		}
		market_liberal = {
			popularity = 5
		}
		social_liberal = {
			popularity = 10
		}
		social_democrat = {
			popularity = 15
		}
		progressive = {
			popularity = 0
		}
		democratic_socialist = {
			popularity = 0
		}
		communist = {
			popularity = 0
		}
	}
	
	ruling_party = conservative
	last_election = "1998.7.26"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Hun Sen"
	picture = "Hun_Sen.dds"
	ideology = fiscal_conservative
}
create_country_leader = {
	name = "Norodom Ranariddh"
	picture = "Norodom_Ranariddh.dds"
	ideology = absolute_monarchist
}
create_country_leader = {
	name = "Sam Rainsy"
	picture = "Sam_Rainsy.dds"
	ideology = social_democrat_ideology
}
create_country_leader = {
	name = "Khem Veasna"
	picture = "Khem_Veasna.dds"
	ideology = green
}
create_country_leader = {
	name = "Lon Rith"
	picture = "Lon_Rith.dds"
	ideology = libertarian
}
create_country_leader = {
	name = "Kem Sokha"
	picture = "Kem_Sokha.dds"
	ideology = liberalist
}
create_corps_commander = {
	name = "Tea Banh"
	picture = "generals/Tea_Banh.dds"
	skill = 1
}
create_corps_commander = {
	name = "Ke Kim Yanh"
	picture = "generals/Ke_Kim_Yan.dds"
	skill = 1
}
create_navy_leader = {
	name = "Tea Vinh"
	picture = "admirals/Tea_Vinh.dds"
	skill = 1
}
2006.10.1 = { add_ideas = { limited_conscription } }
2013.7.28 = {
	set_politics = {
		parties = {
			islamist = { popularity = 0 }
			nationalist = { popularity = 1 }
			reactionary = { popularity = 5 }
			conservative = { popularity = 48 }
			market_liberal = { popularity = 1 }
			social_liberal = { popularity = 41 }
			social_democrat = { popularity = 1 }
			progressive = { popularity = 1 }
			democratic_socialist = { popularity = 1 }
			communist = { popularity = 1 }
		}
		ruling_party = conservative
		last_election = "2013.7.28"
		election_frequency = 48
		elections_allowed = yes
	}
}
2017.1.1 = {

oob = "CBD_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	night_vision_1 = 1
	
	#For templates
	infantry_weapons = 1
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	Anti_Air_0 = 1
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	MBT_1 = 1
	util_vehicle_equipment_0 = 1
	Rec_tank_0 = 1
	
	landing_craft = 1


}

add_ideas = {
	pop_050
	crippling_corruption
	buddism
	gdp_2
		fast_growth
		defence_02
	edu_01
	health_01
	social_01
	bureau_01
	police_04
	volunteer_army
	volunteer_women
	farmers
	oligarchs
	international_bankers
	civil_law
}
set_country_flag = gdp_2
set_country_flag = negative_farmers

#Nat focus
	complete_national_focus = bonus_tech_slots

add_opinion_modifier = { target = CHI modifier = bamboo_network }


set_politics = {

	parties = {
		democratic = { 
			popularity = 35
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 47
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 18
		}
	}
	
	ruling_party = communism
	last_election = "2013.07.28"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Hun Sen"
	desc = ""
	picture = "CAB_Hun_Sen.dds"
	ideology = Autocracy
	traits = {
		guerrilla_leader
		emerging_Autocracy
		pro_china
		sly
		ruthless
	}
}
create_field_marshal = {
	name = "Tea Banh"
	picture = "Portrait_Tea_Banh.dds"
	traits = { old_guard defensive_doctrine }
	skill = 4
}

create_field_marshal = {
	name = "Soeung Samnang"
	picture = "Portrait_Soeung_Samnang.dds"
	traits = { logistics_wizard }
	skill = 4
}

create_corps_commander = {
	name = "Meas Sophea"
	picture = "Portrait_Meas_Sophea.dds"
	traits = { panzer_leader ranger }
	skill = 4
}

create_corps_commander = {
	name = "Sao Sokha"
	picture = "Portrait_Sao_Sokha.dds"
	traits = { urban_assault_specialist }
	skill = 4
}

create_corps_commander = {
	name = "So Phanni"
	picture = "Portrait_So_Phanni.dds"
	traits = {  }
	skill = 3
}

create_corps_commander = {
	name = "Eng Hie"
	picture = "Portrait_Eng_Hie.dds"
	traits = {  }
	skill = 3
}

create_navy_leader = {
	name = "Tea Vinh"
	picture = "Portrait_Tea_Vinh.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 3
}
}