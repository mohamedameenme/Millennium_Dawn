﻿capital = 439

oob = "RAJ_2000"

set_convoys = 700
set_stability = 0.5

set_country_flag = country_language_english
set_country_flag = country_language_hindi

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	missile_destroyer_2 = 1
	missile_destroyer_3 = 1


	modern_carrier_0 = 1
	modern_carrier_1 = 1
	modern_carrier_2 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
	
}

give_guarantee = MLD
give_guarantee = SRI
give_guarantee = BHU
give_guarantee = NEP

add_ideas = {
	population_growth_rapid
	idea_RAJ_south_asian_tiger
	commonwealth_of_nations_member
}

set_politics = {

	parties = {
		
		islamist = {
			popularity = 5
		}
		reactionary = {
			popularity = 5
		}
		conservative = {
			popularity = 41
		}
		market_liberal = {
			popularity = 6
		}
		social_liberal = {
			popularity = 22
		}
		social_democrat = {
			popularity = 2
		}
		progressive = {
			popularity = 2
		}
		democratic_socialist = {
			popularity = 2
		}
		communist = {
			popularity = 15
		}
	}
	
	ruling_party = conservative
	last_election = "1999.10.3"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Atal B. Vajpayee"
	picture = "Atal_Bihari_Vajpayee.dds"
	ideology = right_wing_conservative 
}

create_country_leader = {
	name = "Prem Singh"
	picture = "Prem_Singh.dds"
	ideology = democratic_socialist_ideology 
}

create_country_leader = {
	name = "Princely Council"
	picture = "Council_Of_Princes.dds"
	ideology = absolute_monarchist 
}

create_country_leader = {
	name = "Sonia Ghandi"
	picture = "Sonia_Ghandi.dds"
	ideology = liberalist  
}

create_country_leader = {
	name = "Harkishan Singh Surjeet"
	picture = "Harkishan_Singh_Surjeet.dds"
	ideology = marxist   
}

create_country_leader = {
	name = "Pravin Togadia"
	picture = "Pravin_Togadia.dds"
	ideology = proto_fascist   
}

create_country_leader = {
	name = "Bal Thackeray"
	picture = "Bal_Thackeray.dds"
	ideology = rexist   
}

create_country_leader = {
	name = "K. M. Kader Mohideen"
	picture = "K_M_Kader_Mohideen.dds"
	ideology = islamic_republican   
}

create_country_leader = {
	name = "Uma Bharti"
	picture = "Uma_Bharti.dds"
	ideology = counter_progressive_democrat   
}

create_country_leader = {
	name = "Nagabhairava Jaya Prakash Narayana"
	picture = "Nagabhairava_Jaya_Prakash_Narayana.dds"
	ideology = progressive_ideology    
}

create_country_leader = {
	name = "Parth Shah"
	picture = "Parth_Shah.dds"
	ideology = libertarian    
}

create_country_leader = {
	name = "A. Sayeed"
	picture = "A_Sayeed.dds"
	ideology = social_democrat_ideology     
}


2012.11.17 = {
	create_country_leader = {
		name = "Uddhav Thackeray"
		picture = "Uddhav_Thackeray.dds"
		ideology = rexist
	}
}

2014.5.1 = {
	create_country_leader = {
		name = "Narendra Modi"
		picture = "Narendra_Modi.dds"
		ideology = right_wing_conservative 
	}
	create_country_leader = {
		name = "Rahul Ghandi"
		picture = "Rahul_Ghandi.dds"
		ideology = liberalist  
	}
	
	create_country_leader = {
		name = "Prakash Karat"
		picture = "Prakash_Karat.dds"
		ideology = marxist  
	}

	set_politics = {
		parties = {
			islamist = {
				popularity = 4
			}
			reactionary = {
				popularity = 4
			}
			conservative = {
				popularity = 34
			}
			market_liberal = {
				popularity = 4
			}
			social_liberal = {
				popularity = 34
			}
			social_democrat = {
				popularity = 2
			}
			progressive = {
				popularity = 2
			}
			democratic_socialist = {
				popularity = 2
			}
			communist = {
				popularity = 15
			}
		}
		last_election = "2014.5.7"
		ruling_party = conservative
		elections_allowed = yes
	}
}