﻿capital = 279

oob = "CHL_2000"

set_convoys = 5
set_stability = 0.5

set_country_flag = country_language_spanish

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_steady
}

set_politics = {

	parties = {
		nationalist = {
			popularity = 3
		}
		reactionary = {
			popularity = 3
		}
		conservative = {
			popularity = 30
		}
		market_liberal = {
			popularity = 6
		}
		social_liberal = {
			popularity = 3
		}
		social_democrat = {
			popularity = 5
		}
		progressive = {
			popularity = 10
		}
		democratic_socialist = {
			popularity = 10
		}
		communist = {
			popularity = 20
		}
	}
	
	ruling_party = conservative
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Eduardo Frei Ruiz-Tagle"
	picture = "Ecuardo_Frei_Ruiz_Tagle.dds"
	ideology = christian_democrat
}

create_country_leader = {
	name = "Ricardo Lagos"
	picture = "Ricardo_Lagos.dds"
	ideology = liberalist
}
	
create_country_leader = {
	name = "Michelle Bachelet"
	picture = "Michelle_Bachelet.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Maria Angelica Basualdo"
	picture = "Maria_Angelica_Basualdo.dds"
	ideology = proto_fascist
}

create_country_leader = {
	name = "Guillermo Teillier"
	picture = "Guillermo_Teillier.dds"
	ideology = marxist
}

create_country_leader = {
	name = "Pablo Rodriguez Grez"
	picture = "Pablo_Rodriguez_Grez.dds"
	ideology = fascist_ideology
}



2006.3.11 = {
	set_politics = {
		ruling_party = democratic_socialist
		elections_allowed = yes
	}
}

2016.1.1 = {
	set_politics = {
		last_election = "2014.3.11"
		elections_allowed = yes
	}
}

create_corps_commander = {
	name = "Iván González López"
	picture = "generals/Ivan_Lopez.dds"
	skill = 1
}

create_corps_commander = {
	name = "Ricardo Martínez Menanteau"
	picture = "generals/Ricardo_Menanteau.dds"
	skill = 1
}

create_corps_commander = {
	name = "Guido Montini Gómez"
	picture = "generals/Guido_Gomez.dds"
	skill = 1
}
2017.1.1 = {

oob = "CHL_2017"

add_ideas = {
	pop_050
	modest_corruption
	gdp_6
	rio_pact_member
	christian
		stagnation
		defence_02
	export_economy
	edu_03
	health_03
	social_03
	bureau_02
	police_02
	partial_draft_army
	volunteer_women
	industrial_conglomerates
	international_bankers
	landowners
	civil_law
	cartels_2
}
set_country_flag = gdp_6
set_country_flag = TPP_Signatory
set_country_flag = positive_industrial_conglomerates
set_country_flag = positive_international_bankers
set_country_flag = positive_landowners

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	night_vision_1 = 1
	night_vision_2 = 1
	night_vision_3 = 1
	
	#For templates
	infantry_weapons = 1
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1	
	Heavy_Anti_tank_0 = 1	
	gw_artillery = 1	
	SP_arty_equipment_0 = 1	
	Anti_Air_0 = 1	
	Early_APC = 1	
	APC_1 = 1	
	IFV_1 = 1	
	MBT_1 = 1	
	util_vehicle_equipment_0 = 1	

	landing_craft = 1
}

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4K_GDPC_slot
	complete_national_focus = Generic_15K_GDPC_slot
	complete_national_focus = Generic_4_IC_slot
	complete_national_focus = Generic_8_IC_slot
	complete_national_focus = Generic_16_IC_slot

set_politics = {

	parties = {
		democratic = { 
			popularity = 40
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 10
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 50
		}
	}
	
	ruling_party = democratic
	last_election = "2014.3.11"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Michelle Bachelet"
	desc = "POLITICS_ARTURO_ALESSANDRI_DESC"
	picture = "CHL_Michelle_Bachelet.dds"
	expire = "2050.1.1"
	ideology = conservatism
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Humberto Oviedo Arriagada"
	picture = "Portrait_Humberto_Oviedo.dds"
	traits = { old_guard organisational_leader }
	skill = 3
}

create_field_marshal = {
	name = "Ricardo Martínez Menanteau"
	picture = "Portrait_Ricardo_Martinez.dds"
	traits = { thorough_planner }
	skill = 2
}

create_field_marshal = {
	name = "Jorge Robles Mella"
	picture = "Portrait_Jorge_Robles_Mella.dds"
	traits = { logistics_wizard }
	skill = 4
}

create_corps_commander = {
	name = "Ángelo Hernández"
	picture = "Portrait_Hernandez.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Ramón Oyarzún"
	picture = "Portrait_Oyarzun.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Rodrigo Urrutia Oyarzún"
	picture = "Portrait_Rodrigo_Urrutia.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Rodrigo Goicochea"
	picture = "Portrait_Rodrigo_Goicochea.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Javier Abarzúa"
	picture = "Portrait_Abarzua.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Germán Moreno"
	picture = "Portrait_Moreno.dds"
	traits = { commando }
	skill = 1
}

create_corps_commander = {
	name = "Luis Farías Gallardo"
	picture = "Portrait_Luis_Farias.dds"
	traits = { trickster }
	skill = 2
}

create_corps_commander = {
	name = "Luis Chamorro Heilig"
	picture = "Portrait_Luis_Heilig.dds"
	traits = { commando }
	skill = 2
}

create_corps_commander = {
	name = "Felipe Arancibia Clavel"
	picture = "Portrait_Felipe_Clavel.dds"
	traits = { trait_engineer }
	skill = 2
}

create_corps_commander = {
	name = "Luis Espinoza Arenas"
	picture = "Portrait_Luis_Arenas.dds"
	traits = { panzer_leader }
	skill = 2
}

create_corps_commander = {
	name = "Jorge Peña Leiva"
	picture = "Portrait_Jorge_Pena.dds"
	traits = { jungle_rat }
	skill = 1
}

create_corps_commander = {
	name = "Schafik Nazal Lázaro"
	picture = "Portrait_Schafik_Lazaro.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Miguel Alfonso Bellet"
	picture = "Portrait_Miguel_Alfonso_Bellet.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Andrés Fuentealba Gómez"
	picture = "Portrait_Andres_Fuentealba.dds"
	traits = { fortress_buster }
	skill = 1
}

create_corps_commander = {
	name = "Hernán Araya Santis"
	picture = "Portrait_Hernan_Araya.dds"
	traits = { trickster }
	skill = 1
}

create_corps_commander = {
	name = "Javier Iturriaga Del Campo"
	picture = "Portrait_Javier_Iturriaga.dds"
	traits = { urban_assault_specialist }
	skill = 1
}

create_corps_commander = {
	name = "Sergio Retamal"
	picture = "Portrait_Sergio_Retamal.dds"
	traits = { hill_fighter }
	skill = 1
}

create_corps_commander = {
	name = "Ernesto Tejos Méndez"
	picture = "Portrait_Ernesto_Tejos.dds"
	traits = { trait_mountaineer }
	skill = 1
}

create_corps_commander = {
	name = "Fernando San Cristóbal Schott"
	picture = "Portrait_Fernando_San_Cristobal.dds"
	traits = { ranger }
	skill = 1
}

create_corps_commander = {
	name = "Gustavo Núñez Kocher"
	picture = "Portrait_Gustavo_Nunez.dds"
	traits = { commando }
	skill = 1
}

create_navy_leader = {
	name = "Enrique Larrañaga Martin"
	picture = "Portrait_Enrique_Martin.dds"
	traits = { old_guard_navy superior_tactician }
	skill = 4
}

create_navy_leader = {
	name = "Edmundo González Robles"
	picture = "Portrait_Edmundo_Gonzalez.dds"
	traits = { blockade_runner }
	skill = 4
}
}