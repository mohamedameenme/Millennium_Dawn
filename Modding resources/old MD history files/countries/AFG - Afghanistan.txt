﻿capital = 167

oob = "AFG_2000"

set_stability = 0.5

set_country_flag = country_language_dari
set_country_flag = country_language_pashto

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment = 1
	land_Drone_equipment = 1
	combat_eng_equipment = 1
	body_armor = 1
	camouflage = 1
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	
}

add_ideas = {
	population_growth_explosion
	extensive_conscription
	limited_exports
	partially_recognized_state
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

set_politics = {

	parties = {
		islamist = {
			popularity = 60
		}
		conservative = {
			popularity = 10
		}
		market_liberal = {
			popularity = 5
		}
		social_liberal = {
			popularity = 5
		}
		social_democrat = {
			popularity = 5
		}
		communist = {
			popularity = 5
		}
	}
	
	ruling_party = islamist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Mohammed Omar"
	ideology = islamic_authoritarian
	picture = "Mohammed_Omar.dds"
}

create_country_leader = {
	name = "Hamid Karzai"
	ideology = fiscal_conservative
	picture = "Hamid_Karzai.dds"
}
create_field_marshal = {
	name = "Mohammad Sharif Yaftali"
	picture = "generals/Portrait_Mohammad_Yaftali.dds"
	traits = { old_guard organisational_leader }
	skill = 3
	}

create_field_marshal = {
	name = "Mohammad Afzal Aman"
	picture = "generals/Portrait_Mohammad_Afzal_Zaman.dds"
	traits = { fast_planner }
	skill = 2
	}

create_field_marshal = {
	name = "Abdul Wahab Wardak"
	picture = "generals/Portrait_Abdul_Wahab_Wardak.dds"
	traits = { logistics_wizard }
	skill = 2
	}

create_corps_commander = {
	name = "Murad Ali Murad"
	picture = "generals/Portrait_Murad_Ali_Murad.dds"
	traits = { panzer_leader }
	skill = 3
	}

create_corps_commander = {
	name = "Dawood Shah Wafadar"
	picture = "generals/Portrait_Dawood_Shah_Wafadar.dds"
	traits = { commando hill_fighter }
	skill = 2
	}

create_corps_commander = {
	name = "Mohammad Zaman Waziri"
	picture = "generals/Portrait_Mohammad_Waziri.dds"
	traits = { trait_engineer }
	skill = 2
	}

create_corps_commander = {
	name = "Taj Mohammad Jahid"
	picture = "generals/Portrait_Taj_Mohammad_Jahid.dds"
	traits = { fortress_buster }
	skill = 2
	}

create_corps_commander = {
	name = "Sayed Malook"
	picture = "generals/Portrait_Sayed_Malook.dds"
	traits = { hill_fighter }
	skill = 2
	}

create_corps_commander = {
	name = "Dawlat Waziri"
	picture = "generals/Portrait_Dawlat_Waziri.dds"
	traits = { trickster }
	skill = 2
	}

create_corps_commander = {
	name = "Roshan Safi"
	picture = "generals/Portrait_Roshan_Safi.dds"
	traits = { ranger }
	skill = 1
	}

create_corps_commander = {
	name = "Aminullah Karim"
	picture = "generals/Portrait_Aminullah_Karim.dds"
	traits = { trickster trait_engineer }
	skill = 2
	}

create_navy_leader = {
	name = "Mohammad Sharif Yaftali"
	picture = "admirals/Portrait_Mohammad_Yaftali.dds"
	traits = {  }
	skill = 3
	}
2004.1.1 = {
	oob = "AFG_2016"
	set_party_name = {
		ideology = islamist
		name = AFG_islamist_party_islamic
		long_name = AFG_islamist_party_islamic
	}
	set_politics = {
		parties = {
			islamist = {
				popularity = 10
			}
			conservative = {
				popularity = 10
			}
			social_democrat = {
				popularity = 18
			}
			market_liberal = {
				popularity = 1
			}
			communist = {
				popularity = 10
			}
			reactionary = {
				popularity = 10
			}
		}
		ruling_party = conservative
		elections_allowed = yes
		last_election = "2003.6.1"
	}
	remove_ideas = {
		partially_recognized_state
	}
}
2017.1.1 = {

oob = "AFG_2017"

set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#Basic Rifles
	infantry_weapons = 1
	
	
	#Give some night vision
	night_vision_1 = 1
	
	#Old Radios
	command_control_equipment = 1
	
	#Old artillery
	gw_artillery = 1
	
	#Needed for SPAA template
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	
	#Needed for Mech/Arm Inf/Arm templates
	Early_APC = 1
	APC_1 = 1
	IFV_1 = 1
	Rec_tank_0 = 1
	MBT_1 = 1
	
	#Needed for Mot
	util_vehicle_equipment_0 = 1
	
	#Needed for HAT and HIW
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	
}

add_ideas = {
	pop_050
	crippling_corruption
	gdp_1
	sunni
	youth_radicalization
    	stagnation
		defence_06
	edu_02
	health_02
	social_01
	bureau_02
	police_04
	volunteer_army
	volunteer_women
	Resolute_Support_Mission
	Major_Non_NATO_Ally
	USA_usaid #https://explorer.usaid.gov/aid-dashboard.html
	The_Ulema
	international_bankers
	farmers
	hybrid
}
set_country_flag = gdp_1
set_country_flag = Major_Non_NATO_Ally
set_country_flag = Major_Importer_US_Arms
set_country_flag = positive_international_bankers
set_country_flag = negative_farmers

#Nat focus
	complete_national_focus = bonus_tech_slots

	set_politics = {

	parties = {
		democratic = { 
			popularity = 40
		}

		fascism = {
			popularity = 30
		}
		
		communism = {
			popularity = 20
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 10
		}
	}
	
	ruling_party = democratic
	last_election = "2014.4.5"
	election_frequency = 60
	elections_allowed = yes
	}

#Leader of the Taliban
create_country_leader = {
	name = "Akhtar Muhammad Mansor"
	desc = "POLITICS_MOHAMMED_ZAHIR_SHAH_DESC"
	picture = "AFG-Salafi-akhtar-muhammad-mansor.dds"
	expire = "2050.1.1"
	ideology = Caliphate
	traits = {
		#
		}
	}

create_country_leader = {
	name = "Ashraf Ghani"
	desc = "POLITICS_MOHAMMED_ZAHIR_SHAH_DESC"
	picture = "AFG-Ashraf-Ghani.dds"
	expire = "2050.1.1"
	ideology = conservatism
	traits = {
		#
		}
	}
}
