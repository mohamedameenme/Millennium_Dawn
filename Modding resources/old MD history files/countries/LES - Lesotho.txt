﻿capital = 818

oob = "LES_2000"

set_convoys = 10
set_stability = 0.5

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1

	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_rapid
	african_union_member
	commonwealth_of_nations_member
}

set_politics = {

	parties = {
		progressive = {
			popularity = 10
		}
		democratic_socialist = {
			popularity = 38
		}
		social_democrat = {
			popularity = 39
		}
		monarchist = {
			popularity = 3
		}
		conservative = {
			popularity = 10
		}
	}
	
	ruling_party = social_democrat
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Mothetjoa Metsing" #Green
	picture = "Mothetjoa_Metsing.dds"
	ideology = green
}

create_country_leader = {
	name = "Tom Thabane" #democraticsocialist
	picture = "Tom_Thabane.dds"
	ideology = democratic_socialist_ideology
}

create_country_leader = {
	name = "Pakalitha Mosisili" #socialdemocrat
	picture = "Pakalitha_Mosisili.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Letsie III of Lesotho" #monarchism
	picture = "Letsie_III.dds"
	ideology = absolute_monarchist
}

create_country_leader = {
	name = "Thesele Maseribane" #conservatism
	picture = "Thesele_Maseribane.dds"
	ideology = right_wing_conservative
}
