﻿capital = 446

oob = "EGY_2000"

set_convoys = 150
set_stability = 0.5

set_country_flag = country_language_arabic

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 2
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	frigate2 = 2
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	
	
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_steady
	limited_conscription
	arab_league_member
	african_union_member
}

set_country_flag = dominant_religion_islam
set_country_flag = sunni_islam

set_politics = {

	parties = {
		islamist = {
			popularity = 25
		}
		nationalist = {
			popularity = 35
		}
		reactionary = {
			popularity = 10
		}
		conservative = {
			popularity = 20
		}
		social_liberal = {
			popularity = 5
		}
		communist = {
			popularity = 5
		}
	}
	
	ruling_party = nationalist
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Husni Mubarak"
	picture = "Husni_Mubarak.dds"
	ideology = autocrat
}
create_country_leader = {
	name = "Abdel Fattah as Sisi"
	picture = "Abdel_Fattah_as_Sisi.dds"
	ideology = fiscal_conservative
}
create_country_leader = {
	name = "Younes Makhioun"
	picture = "Younes_Makhioun.dds"
	ideology = counter_progressive_democrat
}
create_country_leader = {
	name = "Ahmed Shafik"
	picture = "Ahmed_Shafik.dds"
	ideology = moderate
}
create_country_leader = {
	name = "Mohamed Morsi"
	picture = "Mohamed_Morsi.dds"
	ideology = islamic_authoritarian
}
create_country_leader = {
	name = "Mariam Milad"
	picture = "Mariam_Milad.dds"
	ideology = libertarian
}
create_country_leader = {
	name = " Adly Mahmoud Mansour"
	picture = "Adly_Mansour.dds"
	ideology = social_democrat_ideology
}
create_country_leader = {
	name = "Ahmed Fuad Farouk II"
	picture = "Fuad_Farouk.dds"
	ideology = absolute_monarchist                              
}
create_country_leader = {
	name = "Mortada Mansour"
	picture = "Mortada_Mansour.dds"
	ideology = national_socialist
}
create_country_leader = {
	name = "Syed Abdel Aal"
	picture = "Syed_Abd_El-Ghani.dds"
	ideology = green
}
create_country_leader = {
	name = "Salah Adli"
	picture = "Salah_Adli.dds"
	ideology = marxist
}
create_country_leader = {
	name = "Syed Abd El-Ghani"
	picture = "Syed_Abd_El-Ghani.dds"
	ideology = democratic_socialist_ideology
}

create_corps_commander = { 
	name = "Mahmoud Hejazi"
	picture = "generals/Mahmoud_Hejazi.dds"
	skill = 2
}
create_corps_commander = { 
	name = "Mohamed Tantawi"
	picture = "generals/Mohamed_Tantawi.dds"
	skill = 1
}
create_corps_commander = { 
	name = "Ahmed Mohamed"
	picture = "generals/Ahmed_Mohamed.dds"
	skill = 1
}
create_corps_commander = { 
	name = "Sedki Sobhi" 
	picture = "generals/Sedki_Sobhi.dds"
	skill = 1
}
create_corps_commander = { 
	name = "Mohamed Samir" 
	picture = "generals/Mohamed_Samir.dds"
	skill = 1
}
create_corps_commander = { 
	name = "Osama al-Jamal" 
	picture = "generals/Osama_Al_Jamal.dds"
	skill = 1
}
create_corps_commander = { 
	name = "Younes el-Masri" 
	picture = "generals/Younes_el-Masri.dds"
	skill = 1
}

create_navy_leader = { 
	name = "Ahmed Hassan Saeed" 
	picture = "admirals/Ahmed_H_Saeed"
	skill = 2
}
create_navy_leader = { 
	name = "Mohamed Abdel Aziz"  
	picture = "admirals/Mohamed_Abdel_Aziz"
	skill = 1
}
create_navy_leader = { 
	name = "Osama El-Gendi"  
	picture = "admirals/Osama_El_Gendi"
	skill = 1
}
create_navy_leader = { 
	name = "Osama Raba'ai"
	picture = "admirals/Osama_Raba_ai"
	skill = 1
}
create_navy_leader = { 
	name = "Mohab Mamish"  
	picture = "admirals/Mohab_Mamish" 
	skill = 1
}

2014.1.1 = {
	set_politics = {

		parties = {
			islamist = {
				popularity = 36
			}
			nationalist = {
				popularity = 5
			}
			reactionary = {
				popularity = 5
			}
			conservative = {
				popularity = 40
			}
			social_liberal = {
				popularity = 5
			}
			communist = {
				popularity = 9
			}
		}
		
		ruling_party = conservative
		last_election = "2013.6.1"
		election_frequency = 48
		elections_allowed = no
	}
}

2014.6.8 = {	#General al-Sisi takes power
	add_stability = -0.1
}