﻿capital = 47

oob = "GRE_2000"

set_convoys = 160
set_stability = 0.5

set_country_flag = country_language_greek

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	AT_upgrade_1 = 1
	Heavy_Anti_tank_1 = 1
	Anti_tank_1 = 1 
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	corvette1 = 1
	corvette2 = 1
	frigate_1 = 1
	frigate_2 = 1
	missile_frigate_1 = 1
	missile_frigate_2 = 1
	missile_frigate_2 = 1
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1

	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_decline
	idea_eu_member
	limited_conscription
}

give_guarantee = CYP
give_military_access = CYP
add_opinion_modifier = { target = TUR modifier = rival }

set_politics = {

	parties = {
		fascist = {
			popularity = 2
		}
		reactionary = {
			popularity = 2
		}
		conservative = {
			popularity = 25
		}
		market_liberal = {
			popularity = 6
		}
		social_liberal = {
			popularity = 5		
		}
		social_democrat = {
			popularity = 30
		}
		progressive = {
			popularity = 10
		}
		democratic_socialist = {
			popularity = 10
		}
		communist = {
			popularity = 10
		}
	}
	
	ruling_party = social_democrat
	last_election = "1997.1.1"
	election_frequency = 48
	elections_allowed = yes
}

create_country_leader = {
	name = "Nikolaos Michaloliakos"
	picture = "Nikolaos_Michaloliakos.dds"
	ideology = national_socialist
}

create_country_leader = {
	name = "Georgios Karatzaferis"
	picture = "Georgios_Karatzaferis.dds"
	ideology = national_democrat 
}
	
create_country_leader = {
	name = "Kostas Karamanlis"
	picture = "Kostas_Karamanlis.dds"
	ideology = constitutionalist 
}
	
create_country_leader = {
	name = "Constantine II"
	picture = "Constantine_II.dds"
	ideology = absolute_monarchist
}
	
create_country_leader = {
	name = "Vassilis Leventis"
	picture = "Vassilis_Leventis.dds"
	ideology = libertarian
}
	
create_country_leader = {
	name = "Stavros Theodorakis"
	picture = "Stavros_Theodorakis.dds"
	ideology = moderate
}

create_country_leader = {
	name = "Costas Simitis"
	picture = "Costas_Simitis.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Alekos Alavanos"
	picture = "Alekos_Alavanos.dds"
	ideology = democratic_socialist_ideology
}
	
	
create_country_leader = {
	name = "Aleka Papariga"
	picture = "Aleka_Papariga.dds"
	ideology = marxist
}

2002.1.1 = {
	add_ideas = the_euro
}

2016.1.1 = {
create_country_leader = {
		name = "Kyriakos Mitsotakis"
		picture = "Kyriakos_Mitsotakis.dds"
		ideology = constitutionalist
	}
	
create_country_leader = {
	name = "Fofi Gennimata"
	picture = "Fofi_Gennimata.dds"
	ideology = social_democrat_ideology
}
	
create_country_leader = {
	name = "Alexis Tsipras"
	picture = "Alexis_Tsipras.dds"
	ideology = democratic_socialist_ideology
}
	
create_country_leader = {
	name = "Dimitris Koutsoumpas"
	picture = "Dimitris_Koutsoumpas.dds"
	ideology = leninist
}
	
	set_politics = {
		parties = {
			fascist = { popularity = 7 }
			nationalist = { popularity = 1 }
			conservative = { popularity = 30 }
			market_liberal = { popularity = 5 }
			social_liberal = { popularity = 5 }
			democratic_socialist = { popularity = 32 }
			communist = { popularity = 9 }
		}
		ruling_party = democratic_socialist
		last_election = "2015.9.20"
		election_frequency = 48
		elections_allowed = yes
	}
}
