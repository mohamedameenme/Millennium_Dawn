﻿capital = 756

oob = "CAM_2000"

set_convoys = 20
set_stability = 0.5

set_country_flag = country_language_english
set_country_flag = country_language_french

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_explosion
	commonwealth_of_nations_member
	african_union_member
}

set_politics = {

	parties = {
		islamist = {
			popularity = 1
		}
		nationalist = {
			popularity = 1
		}
		reactionary = {
			popularity = 1
		}
		conservative = {
			popularity = 20
		}
		market_liberal = {
			popularity = 2
		}
		social_liberal = {
			popularity = 2
		}
		social_democrat = {
			popularity = 30
		}
		progressive = {
			popularity = 2
		}
		democratic_socialist = {
			popularity = 40
		}
		communist = {
			popularity = 1
		}
	}
	
	ruling_party = democratic_socialist
	last_election = "1997.10.12"
	election_frequency = 60
	elections_allowed = yes
}

create_country_leader = {
	name = "Paul Biya"
	picture = "Paul_Biya.dds"
	ideology = democratic_socialist_ideology
}

2017.1.1 = {

oob = "CAM_2017"

# Starting tech
set_technology = { 
 legacy_doctrines = 1 
 modern_blitzkrieg = 1 
 forward_defense = 1 
 encourage_nco_iniative = 1 
 air_land_battle = 1
	#For templates
	infantry_weapons = 1
	combat_eng_equipment = 1
	command_control_equipment = 1
	Anti_tank_0 = 1
	Heavy_Anti_tank_0 = 1
	gw_artillery = 1
	SP_arty_equipment_0 = 1
	Anti_Air_0 = 1
	Rec_tank_0 = 1
	util_vehicle_equipment_0 = 1

	landing_craft = 1
}

add_ideas = {
	pop_050
	rampant_corruption
	christian
	gdp_2
		fast_growth
		defence_02
	edu_02
	health_01
	bureau_03
	police_04
	volunteer_army
	volunteer_women
	international_bankers
	farmers
	hybrid
}
set_country_flag = gdp_2
set_country_flag = positive_international_bankers
set_country_flag = negative_farmers

#Nat focus
	complete_national_focus = bonus_tech_slots
	complete_national_focus = Generic_4_IC_slot



	set_politics = {

	parties = {
		democratic = { 
			popularity = 15
		}

		fascism = {
			popularity = 0
		}
		
		communism = {
			popularity = 0
			#banned = no #default is no
		}
		
		neutrality = { 
			popularity = 85
		}
	}
	
	ruling_party = neutrality
	last_election = "2011.10.9"
	election_frequency = 84
	elections_allowed = yes
}

create_country_leader = {
	name = "Paul Biya"
	desc = ""
	picture = "CAM_Paul_Biya.dds"
	expire = "2065.1.1"
	ideology = Neutral_conservatism
	traits = {
		#
	}
}
create_field_marshal = {
	name = "René Claude Meka"
	picture = "generals/Portrait_Rene_Claude_Meka.dds"
	traits = { organisational_leader }
	skill = 2
}

create_field_marshal = {
	name = "Jean-Calvin Momha"
	picture = "generals/Portrait_Jean_Calvin_Momha.dds"
	traits = { logistics_wizard }
	skill = 1
}

create_corps_commander = {
	name = "Martin Tumenta Chumo"
	picture = "generals/Portrait_Martin_Tumenta_Chumo.dds"
	traits = { panzer_leader }
	skill = 1
}

create_corps_commander = {
	name = "Jacob Kodji"
	picture = "generals/Portrait_Jacob_Kodji.dds"
	traits = { fortress_buster }
	skill = 2
}

create_corps_commander = {
	name = "Ahmed Mahamat"
	picture = "generals/Portrait_Ahmed_Mahamat.dds"
	traits = { commando ranger }
	skill = 1
}

create_corps_commander = {
	name = "Frédéric Ndjonkep Meyomhy"
	picture = "generals/Portrait_Frederic_Ndjonkep_Meyomhy.dds"
	traits = { trait_engineer }
	skill = 1
}

create_navy_leader = {
	name = "Jean Mendoua"
	picture = "admirals/Portrait_Jean_Mendoua.dds"
	traits = { blockade_runner }
	skill = 2
}

}
