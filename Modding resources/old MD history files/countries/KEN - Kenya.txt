﻿capital = 547

oob = "KEN_2000"

set_convoys = 120
set_stability = 0.5

set_country_flag = country_language_english
set_country_flag = country_language_swahili

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	infantry_weapons3 = 1
	infantry_weapons4 = 1
	night_vision_1 = 1
	command_control_equipment = 1
	command_control_equipment1 = 1
	command_control_equipment2 = 1
	command_control_equipment3 = 1
	land_Drone_equipment = 1
	land_Drone_equipment1 = 1
	combat_eng_equipment = 1
	body_armor = 1
	body_armor2 = 1
	camouflage = 1
	
	Early_APC = 1
	MBT_1 = 1
	ENG_MBT_1
	MBT_2 = 1
	ENG_MBT_2 = 1
	MBT_3 = 1
	ENG_MBT_3 = 1
	Rec_tank_0 = 1
	APC_1 = 1
	Air_APC_1 = 1
	APC_2 = 1
	Air_APC_2 = 1
	IFV_1 = 1
	Air_IFV_1 = 1
	IFV_2 = 1
	Air_IFV_2 = 1
	util_vehicle_equipment_0 = 1
	util_vehicle_equipment_1 = 1
	
	gw_artillery = 1
	artillery_1 = 1
	Anti_tank_2 = 1
	Heavy_Anti_tank_2 = 1
	AT_upgrade_2 = 1
	Anti_Air_0 = 1
	SP_Anti_Air_0 = 1
	AA_upgrade_1 = 1
	
	destroyer_1 = 1
	destroyer_2 = 1
	missile_destroyer_1 = 1
	submarine_1 = 1
	attack_submarine_1 = 1
	attack_submarine_2 = 1
	attack_submarine_3 = 1
	
	early_bomber = 1
	cas1 = 1
	cas2 = 1
	early_fighter = 1
	MR_Fighter1 = 1
	MR_Fighter2 = 1
	MR_upgrade_1 = 1
	MR_Fighter3 = 1
	naval_plane1 = 1
	naval_plane2 = 1
	naval_plane3 = 1
	strategic_bomber1 = 1
	strategic_bomber2 = 1
	strategic_bomber3 = 1
	Air_UAV1 = 1
	
	
}

add_ideas = {
	population_growth_rapid
	african_union_member
	commonwealth_of_nations_member
}

set_politics = {

	parties = {
	islamist = {
	popularity = 0
	}
	fascist = {
	popularity = 0
	}
	nationalist = {
	popularity = 16
	}
	reactionary = {
	popularity = 2
	}
	conservative = {
	popularity = 50
	}
	market_liberal = {
	popularity = 2
	}
	social_liberal = {
	popularity = 21
	}
	social_democrat = {
	popularity = 1
	}
	progressive = {
	popularity = 1
	}
	democratic_socialist = {
	popularity = 1
	}
	communist = {
	popularity = 6
	}
	}
	
	ruling_party = conservative
	last_election = "1997.12.29"
	election_frequency = 60
	elections_allowed = yes
}
	
create_country_leader = {
	name = "Daniel Arap Moi"
	picture = "Daniel_Arap_Moi.dds"
	ideology = right_wing_conservative
}

#	create_country_leader = {
#	name = "Mwai Kibaki"
#	picture = "Mwai_Kibaki.dds"
#	ideology = right_wing_conservative
#	}

create_country_leader = {
	name = "Michael Wamalwa"
	picture = "Michael_Wamalwa.dds"
	ideology = moderate
}

create_country_leader = {
	name = "Mwandawiro Mghanga"
	picture = "Mwandawiro_Mghanga.dds"
	ideology = marxist
}

create_country_leader = {
	name = "Katama Mkangi"
	picture = "Katama_Mkangi.dds"
	ideology = libertarian
}

create_country_leader = {
	name = "Moses Wetangula"
	picture = "Moses_Wetangula.dds"
	ideology = liberalist
}

create_country_leader = {
	name = "Raila Odinga"
	picture = "Raila_Odinga.dds"
	ideology = social_democrat_ideology
}

create_country_leader = {
	name = "Uhuru Kenyatta"
	picture = "Uhuru_Kenyatta.dds"
	ideology = counter_progressive_democrat
}

create_country_leader = {
	name = "Musalia Mudavadi"
	picture = "Musalia_Mudavadi.dds"
	ideology = progressive_ideology
}

create_corps_commander = {
	name = "Samson Mwathethe"
	picture = "generals/Samson_Mwathethe.dds"
	skill = 1
}

create_corps_commander = {
	name = "Leonard Ngondi"
	picture = "generals/Leonard_Ngondi.dds"
	skill = 1
}

create_navy_leader = {
	name = "Levi Mghalu"
	picture = "admirals/Levi_Mghalu.dds"
	skill = 1
}

2013.3.4 = {
	set_politics = {
		parties = {
			islamist = { popularity = 0 }
			fascist = { popularity = 0 }
			nationalist = { popularity = 5 }
			reactionary = { popularity = 40 }
			conservative = { popularity = 5 }
			market_liberal = { popularity = 0 }
			social_liberal = { popularity = 20 }
			social_democrat = { popularity = 25 }
			progressive = { popularity = 5 }
			democratic_socialist = { popularity = 0 }
			communist = { popularity = 0 }
		}
		ruling_party = reactionary
		last_election = "2013.3.4"
		election_frequency = 60
		elections_allowed = yes
	}
}