ideas = {
	
	hidden_ideas = {
		
		three_months_of_war = {
		
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				#war_support_factor = 0
			}
		}
		
		ab_penalty_1 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.03
				
			}
			
		}
		ab_penalty_2 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.6
				
			}
			
		}
		ab_penalty_3 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.9
				
			}
			
		}
		ab_penalty_4 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.12
				
			}
			
		}
		ab_penalty_5 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.15
				
			}
			
		}
		ab_penalty_6 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.18
				
			}
			
		}
		ab_penalty_7 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.21
				
			}
			
		}
		ab_penalty_8 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.24
				
			}
			
		}
		ab_penalty_9 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.27
				
			}
			
		}
		ab_penalty_10 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.3
				
			}
			
		}
		ab_penalty_11 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.33
				
			}
			
		}
		ab_penalty_12 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.36
				
			}
			
		}
		ab_penalty_13 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.39
				
			}
			
		}
		ab_penalty_14 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.42
				
			}
			
		}
		ab_penalty_15 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.45
				
			}
			
		}
		ab_penalty_16 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.48
				
			}
			
		}
		ab_penalty_17 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.51
				
			}
			
		}
		ab_penalty_18 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.53
				
			}
			
		}
		ab_penalty_19 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.58
				
			}
			
		}
		ab_penalty_20 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.61
				
			}
			
		}
		ab_penalty_21 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.64
				
			}
			
		}
		ab_penalty_22 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.68
				
			}
			
		}
		ab_penalty_23 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.71
				
			}
			
		}
		ab_penalty_24 = {
			
			allowed = {
				always = yes
			}
			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				war_support_factor = -0.74
				
			}
			
		}
		
	
	}
	
}