ideas = {

	country = {
		article_nine_jap = {
			
			allowed = {
				original_tag = JAP
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				conversion_cost_civ_to_mil_factor = 0.7
				lend_lease_tension = 1.00
				send_volunteers_tension = 1.00
				guarantee_tension = 1.00
				join_faction_tension = 1.00
			}
		}
		JAP_deflation = {
			
			allowed = {
				original_tag = JAP
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				production_speed_buildings_factor = -0.25
				economic_cycles_cost_factor = 0.5
			}
		}
		JAP_support_business = {
		
			picture = foreign_capital
			
			allowed = {
				original_tag = JAP
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				consumer_goods_factor = -0.02
				production_speed_industrial_complex_factor = 0.1
			}
		}
		JAP_reformed_economic_ministry = {
		
			picture = new_deal
			
			allowed = {
				original_tag = JAP
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				economic_cycles_cost_factor = -0.2
				production_factory_max_efficiency_factor = 0.1
			}
		}
		JAP_officer_schools = {
		
			picture = reserve_divisions
			
			allowed = {
				original_tag = JAP
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				Officer_Training_Law_cost_factor = -0.2
				Conscription_Law_cost_factor = -0.1
			}
		}
		JAP_ideological_debate = {
		
			picture = reserve_divisions
			
			allowed = {
				original_tag = JAP
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				drift_defence_factor = -0.2
			}
		}
		JAP_devalued_personal_savings = {
		
			picture = financial_crisis
			
			allowed = {
				original_tag = JAP
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1
			
			modifier = {
				consumer_goods_factor = -0.05
				political_power_gain = -0.25
			}
		}
	}
}