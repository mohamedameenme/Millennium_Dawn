ideas = {

	country = {

		divided_syria = {
			
			allowed = {
				original_tag = SYR
			}
	
			modifier = {
				conscription_factor = -0.4
				MONTHLY_POPULATION = -0.5
			}
		}
		
		syrian_social_nationalists = {
			
			allowed = {
				original_tag = SYR
			}
			
			allowed_civil_war = {
				has_government = nationalist
			}
	
			modifier = {
				conscription_factor = 0.1
				nationalist_drift = 0.05
			}
		}
		
		syrian_social_nationalists_inc = {
			
			allowed = {
				original_tag = SYR
			}
			
			allowed_civil_war = {
				has_government = nationalist
			}
			
			modifier = {
				conscription_factor = 0.15
				nationalist_drift = 0.10
				political_power_factor = -0.10
			}
		}
		
		palestine_liberation_groups = {
			
			allowed = {
				original_tag = SYR
			}
			
			allowed_civil_war = {
				has_government = communism
			}
	
			modifier = {
				conscription_factor = 0.03
				
			}
		}
		
		palestine_liberation_groups_integrated = {
			
			allowed = {
				original_tag = SYR
			}
			
			allowed_civil_war = {
				has_government = communism
			}
	
			modifier = {
				conscription_factor = 0.03
				army_morale_factor = -0.10
				army_org_factor = -0.10
				stability_factor = -0.05
			}
		}
		
		syrian_shiite = {
			
			allowed = {
				original_tag = SYR
			}
			
			allowed_civil_war = {
				has_government = communism
			}
	
			modifier = {
				conscription_factor = 0.05
			}
		}
		
		syrian_shiite_integrated = {
			
			allowed = {
				original_tag = SYR
			}
			
			allowed_civil_war = {
				has_government = communism
			}
	
			modifier = {
				conscription_factor = 0.05
				communism_drift = 0.02
				political_power_factor = -0.10
				stability_factor = -0.05
			}
		}
		
		alawite_high_command = {
		
			allowed = {
				original_tag = SYR
			}
			
			allowed_civil_war = {
				has_government = communism
			}
			
			modifier = {
				military_leader_cost_factor = 0.50
				stability_factor = 0.05
			}
		}
		
		baath_party_banned = {
		
			allowed = {
				original_tag = FSA
			}
			
			allowed_civil_war = {
				NOT = { has_government = communism }
			}
			
			modifier = {
				communism_drift = -0.03
				stability_factor = -0.03
			}
		}
		
		ssnp_party_banned = {
		
			allowed = {
				original_tag = FSA
			}
			
			allowed_civil_war = {
				OR = {
					NOT = { has_government = nationalist }
					NOT = { has_government = communism }
				}
			}
			
			modifier = {
				nationalist_drift = -0.03
				stability_factor = -0.03
			}
		}
		
		salafist_party_banned = {
		
			allowed = {
				original_tag = FSA
			}
			
			allowed_civil_war = {
				NOT = { has_government = fascism }
			}
			
			modifier = {
				fascism_drift = -0.03
				stability_factor = -0.03
			}
		}
		
		true_caliphate = {
		
			allowed = {
				original_tag = NUS
				has_government = fascism
			}
			
			allowed_civil_war = {
				NOT = { has_government = fascism }
			}
			
			modifier = {
				drift_defence_factor = 0.10
			}
		}
		
		global_salafist_recruitment = {
		
			allowed = {
				original_tag = NUS
				has_government = fascism
			}
			
			allowed_civil_war = {
				NOT = { has_government = fascism }
			}
			
			modifier = {
				conscription = 0.01
			}
		}
		
		infiltrate_hezbollah_idea = {
		
			allowed = {	
				has_country_flag = HEZ_Infiltrated_by_SYR
			}
			
			allowed_civil_war = {
				always = no
			}
			
			targeted_modifier = {
				tag = HEZ
				attack_bonus_against = 0.1
			}
		}
		
	}
	
}