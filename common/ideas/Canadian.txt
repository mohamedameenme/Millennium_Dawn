ideas = {
	country = {
	
		#French canadian Hapiness Ideas
		CAN_extremely_high_french_happiness = {
			allowed = {
				always = yes
			}
			visible = {
				
			}
			
			removal_cost = -1
			
			picture = morale_bonus
			
			modifier = {
				stability_factor = 0.15
				political_power_factor = 0.25
			}
		}
		
		CAN_very_high_french_happiness = {
			allowed = {
				always = yes
			}
			visible = {
				
			}
			removal_cost = -1
			
			picture = morale_bonus
			
			modifier = {
				stability_factor = 0.1
				political_power_factor = 0.2
			}
		}
		
		CAN_high_french_happiness = {
			allowed = {
				always = yes
			}
			visible = {
				
			}
			removal_cost = -1
			
			picture = morale_bonus
			
			modifier = {
				stability_factor = 0.05
				political_power_factor = 0.1
			}
		}
		
		CAN_medium_french_happiness = {
			allowed = {
				always = yes
			}
			visible = {
				
			}
			removal_cost = -1
			
			picture = morale_bonus
			
			modifier = {
				political_power_factor = 0.05
			}
		}
		
		CAN_low_french_happiness = {
			allowed = {
				always = yes
			}
			visible = {
				
			}
			removal_cost = -1
			
			picture = morale_bonus
			
			modifier = {
				stability_factor = -0.05
			}
		}
		
		CAN_very_low_french_happiness = {
			allowed = {
				always = yes
			}
			visible = {
				
			}
			removal_cost = -1
			
			picture = morale_bonus
			
			modifier = {
				stability_factor = -0.1
				political_power_factor = -0.25
			}
		}
		
		CAN_extremely_low_french_happiness = {
			allowed = {
				always = yes
			}
			visible = {
				
			}
			removal_cost = -1
			
			picture = morale_bonus
			
			modifier = {
				stability_factor = -0.15
				political_power_factor = -0.5
			}
		}
		
		#Oil Prospects
		CAN_oil_prospects_idea1 = {
			allowed = {
				always = yes
			}
			visible = {
				
			}
			removal_cost = -1
			
			picture = morale_bonus
			
			modifier = {
				local_resources_factor = 0.03
			}
		}
		
		CAN_oil_prospects_idea2 = {
			allowed = {
				always = yes
			}
			visible = {
				
			}
			removal_cost = -1
			
			picture = morale_bonus
			
			modifier = {
				local_resources_factor = 0.075
				consumer_goods_factor = 0.01
			}
		}
		
		#Regular Ideas
		CAN_suncor_energy_idea = {
			allowed = {
				always = yes
			}
			visible = {
				
			}
			removal_cost = -1
			
			picture = morale_bonus
			
			modifier = {
				production_oil_factor = 0.05
				consumer_goods_factor = 0.01
			}
		}
		
		CAN_imperial_oil_idea = {
			allowed = {
				always = yes
			}
			visible = {
				
			}
			removal_cost = -1
			
			picture = morale_bonus
			
			modifier = {
				
			}
		}
		
		CAN_international_companies_idea = {
			allowed = {
				always = yes
			}
			visible = {
				
			}
			removal_cost = -1
			
			picture = morale_bonus
			
			modifier = {
				trade_opinion_factor = 0.35
				trade_laws_cost_factor = -0.05
			}
		}
		
	}

}