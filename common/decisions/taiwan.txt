TAI_category = {
	TAI_coast_guard = {
		allowed = { tag = TAI }

		cost = 25

		available = {
			date > 2000.2.1
		}

		Is_good = yes

		fire_only_once = yes

		complete_effect = {
			effect_tooltip = { navy_experience = 5 }
			hidden_effect = { country_event = { id = taiwan.4 days = 1 } }
		}

		ai_will_do = {
			factor = 1
		}
	}
	TAI_kaoshiung_university = {
		allowed = { tag = TAI }

		cost = 50

		available = {
			date > 2000.2.1
		}

		Is_good = yes

		fire_only_once = yes

		complete_effect = {
			effect_tooltip = {  } ### TODO research bonus for industry
			hidden_effect = { country_event = { id = taiwan.5 days = 1 } }
		}

		ai_will_do = {
			factor = 1
		}
	}
}
