set_leader_VEN = {

	if = { limit = { has_country_flag = set_socialism }

		if = { limit = { check_variable = { socialism_leader = 0 } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Manuel Rosales"
				picture = "Manuel_Rosales.dds"
				ideology = socialism
				traits = {
					western_socialism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { socialism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Francisco Arias Cárdenas"
				picture = "generic.dds"
				ideology = socialism
				traits = {
					western_socialism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { socialism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Omar Barboza"
				picture = "generic.dds"
				ideology = socialism
				traits = {
					western_socialism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Communist-State }
		if = { limit = { check_variable = { Communist-State_leader = 0 } }
			add_to_variable = { Communist-State_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Gabriel Puerta Aponte"
				picture = "generic.dds"
				ideology = Communist-State
				traits = {
					emerging_Communist-State
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Communist-State_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_conservatism }
		if = { limit = { check_variable = { Neutral_conservatism_leader = 0 } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Julio Borges"
				picture = "generic.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Neutral_conservatism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Claudio Fermín"
				picture = "generic.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Neutral_conservatism_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Tomas Guanipa"
				picture = "generic.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_neutral_Social }
		if = { limit = { check_variable = { neutral_Social_leader = 0 } }
			add_to_variable = { neutral_Social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Ramon Jose Velasquez"
				picture = "ramon_jose_velasquez.dds"
				ideology = neutral_Social
				traits = {
					neutrality_neutral_Social
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_Social_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { neutral_Social_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { neutral_Social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Isabel Carmona de Serra"
				picture = "generic.dds"
				ideology = neutral_Social
				traits = {
					neutrality_neutral_Social
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_Social_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_Communism }
		if = { limit = { check_variable = { Neutral_Communism_leader = 0 } }
			add_to_variable = { Neutral_Communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Oswaldo Jimenez"
				picture = "generic.dds"
				ideology = Neutral_Communism
				traits = {
					neutrality_Neutral_Communism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Communism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_Nat_Autocracy }
		if = { limit = { check_variable = { Nat_Autocracy_leader = 0 } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Maj. Gen. Jose Vicente Rangel"
				picture = "generic.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Nat_Autocracy_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Gen. Vladimir Padrino Lopez"
				picture = "Portrait_Vladimir_Padrino_Lopez.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Autocracy }
		if = { limit = { check_variable = { Autocracy_leader = 0 } }
			add_to_variable = { Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Hugo Chávez"
				picture = "hugo_chavez.dds"
				ideology = Autocracy
				traits = {
					emerging_Autocracy
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Autocracy_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { has_country_flag = set_liberalism }
		if = { limit = { check_variable = { liberalism_leader = 0 } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Leopoldo Lopez"
				picture = "generic.dds"
				ideology = liberalism
				traits = {
					western_liberalism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { liberalism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Henrique Capriles"
				picture = "Henrique_Capriles.dds"
				ideology = liberalism
				traits = {
					western_liberalism
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { has_country_flag = set_Neutral_green }
		if = { limit = { check_variable = { Neutral_green_leader = 0 } }
			add_to_variable = { Neutral_green_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Manuel Diaz"
				picture = "generic.dds"
				ideology = Neutral_green
				traits = {
					neutrality_Neutral_green
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_green_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
}