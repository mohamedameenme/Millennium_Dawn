# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

Korean_Conflict_Norths_Perspective = {
	enable = {
		original_tag = NKO
		country_exists = KOR
	}
	abort = {
		OR = {
			NOT = { country_exists = KOR }
			threat > 0.3
		}
	}
	
	ai_strategy = {
		type = conquer
		id = "KOR"
		value = 25
	}
	ai_strategy = {
		type = antagonize
		id = "KOR"
		value = 200
	}
	ai_strategy = {
		type = contain
		id = "KOR"
		value = 200
	}
}
Korean_Conflict_Norths_Perspective_1 = {
	enable = {
		original_tag = NKO
		country_exists = KOR
		threat > 0.3
	}
	abort = {
		NOT = { country_exists = KOR }
	}
	
	ai_strategy = {
		type = conquer
		id = "KOR"
		value = 200
	}
	ai_strategy = {
		type = antagonize
		id = "KOR"
		value = 200
	}
}

North_Korea_China_Relations = {
	enable = {
		original_tag = NKO
		country_exists = CHI
	}
	abort = {
		NOT = { country_exists = CHI }
	}
	
	ai_strategy = {
		type = befriend
		id = "CHI"
		value = 25
	}
}