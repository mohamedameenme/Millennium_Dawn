# Written by Andreas "Cold Evul" Brostrom

# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio



Israel_Palestine_Hostile = {
	enable = {
		original_tag = ISR
		country_exists = HAM
		has_opinion_modifier = HAM_Are_Terrorists
	}
	abort = {
		OR = {
			NOT = { country_exists = HAM }
			NOT = { has_opinion_modifier = HAM_Are_Terrorists }
		}
	}

	ai_strategy = {
		type = contain
		id = "HAM"
		value = 50
	}
	ai_strategy = {
		type = antagonize
		id = "HAM"
		value = 100
	}
	ai_strategy = {
		type = conquer
		id = "HAM"
		value = 50
	}
}

Israeli_Arab_conflict = {
	enable = {
		original_tag = ISR
		has_opinion_modifier = ISR_Arab_Conflict
	}
	abort = {
		always = no
	}
	
	ai_strategy = {
		type = antagonize
		id = "LEB"
		value = 50
	}
	ai_strategy = {
		type = antagonize
		id = "HEZ"
		value = 50
	}
	ai_strategy = {
		type = antagonize
		id = "JOR"
		value = 50
	}
	ai_strategy = {
		type = antagonize
		id = "SYR"
		value = 50
	}
	ai_strategy = {
		type = antagonize
		id = "EGY"
		value = 50
	}
	ai_strategy = {
		type = antagonize
		id = "PER"
		value = 50
	}

	ai_strategy = {
		type = antagonize
		id = "SAU"
		value = 30
	}
	ai_strategy = {
		type = antagonize
		id = "QAT"
		value = 30
	}
	ai_strategy = {
		type = antagonize
		id = "UAE"
		value = 30
	}
	ai_strategy = {
		type = antagonize
		id = "YEM"
		value = 50
	}
	ai_strategy = {
		type = antagonize
		id = "HOU"
		value = 30
	}

}
Arab_Israeli_conflict = {
	enable = {
		OR = {
			original_tag = LEB
			original_tag = HEZ
			original_tag = JOR
			original_tag = SYR
			original_tag = EGY
			original_tag = PER
			
			original_tag = SAU
			original_tag = QAT
			original_tag = UAE
			
			original_tag = YEM
			original_tag = HOU
		}
		has_opinion_modifier = ISR_Arab_Conflict
	}
	abort = {
		OR = {
			NOT = { has_opinion_modifier = ISR_Arab_Conflict }
			NOT = { country_exists = ISR }
		}
	}

	ai_strategy = {
		type = antagonize
		id = "ISR"
		value = 50
	}
}

#Israeli Kurd relation 
Israeli_Kurd_Relations = {
	enable = {
		original_tag = ISR
		KUR = { has_war_with = IRQ }
	}
	abort = {
		ISR = { has_war_with = KUR }
	}
	ai_strategy = {
		type = support
		id = "KUR"
		value = 200
	}
}

#Help FSA or Druze if deal is made
Israel_Intervene_in_Syria = {

	reversed = yes
	
	enable = {
		NOT = { original_tag = ISR }
		OR = {
			original_tag = DRU
			original_tag = FSA
		}
		587 = {
			NOT = { is_core_of = ROOT }
			NOT = { is_claimed_by = ROOT }
		}
	}
	
	abort = {
		OR = {
			has_war_with = ISR
			has_government = fascism
		}
	}
	
	ai_strategy = {
		type = support 
		id = "ISR"
		value = 200
	}
	
	ai_strategy = {
		type = send_volunteers_desire 
		id = "ISR"
		value = 200
	}
	
}

#Take out Hezbollah
Israel_destroy_hezbollah = {

	enable = {
		tag = ISR
		HEZ = {	
			owns_state = 184
		}
	}
	
	abort = {
		HEZ = {
			NOT = { owns_state = 184 }
		}
	}
	
	ai_strategy = {
		type = contain
		id = "HEZ"
		value = 200
	}
	
	ai_strategy = {
		type = antagonize
		id = "HEZ"
		value = 200
	}
	
	ai_strategy = {
		type = conquer
		id = "HEZ"
		value = 200
	}
	
}