# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Angola supports Kabila
ANG_DRC = {

	enable = {
		original_tag = ANG
	}
	abort = {
		has_war_with = DRC
	}

	ai_strategy = {
		type = befriend
		id = "DRC"
		value = 100
	}
	ai_strategy = {
		type = support
		id = "DRC"
		value = 200
	}
	ai_strategy = {
		type = send_volunteers_desire
		id = "DRC"
		value = 200
	}
}