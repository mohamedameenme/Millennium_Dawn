# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Sierra Leonean intervention
ENG_SIE = {
	
	enable = {
		original_tag = ENG
	}
	
	abort = {
		SIE = { 
			OR = {
				NOT = { has_government = democratic }
				NOT = { has_war_with = AFR }
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "SIE"
		value = 50
	}
	
	ai_strategy = {
		type = support
		id = "SIE"
		value = 100
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "SIE"
		value = 200
	}
}

#British support for Afghanistan
ENG_AFG2 = {

	enable = {
		original_tag = ENG
		date < 2017.1.12
	}
	
	abort = {
		date > 2017.1.12
	}

	ai_strategy = {
		type = send_volunteers_desire
		id = "AFG"
		value = -200
	}
}

ENG_AFG = {

	enable = {
		original_tag = ENG
	}
	
	abort = {
		AFG = { 
			OR = { 
				war_with_us = yes
				is_bad_salafist = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "AFG"
		value = 50
	}
	
	ai_strategy = {
		type = support
		id = "AFG"
		value = 150
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "AFG"
		value = 50
	}
}

#UK Arms GNA of Libya
UK_Support_GNA = { 
	
	enable = {
		original_tag = ENG
	}
	
	abort = {
		GNA = { 
			OR = { 
				war_with_us = yes 
				is_bad_salafist = yes
				in_different_faction = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "GNA"
		value = 25
	}
	
	ai_strategy = {
		type = protect 
		id = "GNA"
		value = 25
	}
	
	ai_strategy = {
		type = influence
		id = "GNA"
		value = 25
	}
	
	ai_strategy = {
		type = support
		id = "GNA"
		value = 25
	}
	
}

#UK supports Somalian government against Al-Shabaab
UK_support_SOM = {
	
	enable = {
		original_tag = ENG
		SOM = {
			OR = {
				has_government = democratic
				has_government = neutrality
			}
			has_war_with = SHB
		}
	}
	
	abort = {
		OR = {
			SOM = { NOT = { has_war_with = SHB } }
			SOM = { has_government = fascism }
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "SOM"
		value = 100
	}
	
	ai_strategy = {
		type = support 
		id = "SOM"
		value = 100
	}
	
	ai_strategy = {
		type = protect 
		id = "SOM"
		value = 25
	}
}

#UK supports FSA
ENG_Intervene_FSA = {

	enable = {
		original_tag = ENG
	}
	
	abort = {
		FSA = { 
			OR = { 
				war_with_us = yes 
				is_bad_salafist = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "FSA"
		value = 50
	}
	
	ai_strategy = {
		type = protect 
		id = "FSA"
		value = 50
	}
	
	
	ai_strategy = {
		type = support
		id = "FSA"
		value = 150
	}
}

#Support Pakistan against islamists
ENG_PAK_Civil_War_Relations = {
	enable = {
		original_tag = ENG
		PAK = { 
			AND = { 
				has_civil_war = yes
				NOT = { has_government = fascism }
				has_country_flag = fighting_jihadis
			}
		}
	}
	abort = {
		
	}
	ai_strategy = {
		type = support
		id = "PAK"
		value = 300
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "PAK"
		value = 300
	}
}

#Support Egypt against islamists
ENG_EGY_Civil_War_Relations = {
	enable = {
		original_tag = ENG
		EGY = { 
			AND = { 
				has_civil_war = yes
				NOT = { has_government = fascism }
				has_country_flag = fighting_jihadis
			}
		}
	}
	abort = {
		
	}
	ai_strategy = {
		type = support
		id = "EGY"
		value = 300
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "EGY"
		value = 300
	}
}