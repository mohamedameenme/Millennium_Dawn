# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

Democratic_China_Taiwans_Perspective = {
	enable = {
		original_tag = TAI
		has_government = democratic
		country_exists = CHI
	}
	abort = {
		OR = {
			NOT = { country_exists = CHI }
			OR = {
				NOT = { has_government = democratic }
				NOT = { has_government = neutrality }
			}
		}
	}
	
	ai_strategy = {
		type = antagonize
		id = "CHI"
		value = 200
	}
	ai_strategy = {
		type = contain
		id = "CHI"
		value = 100
	}
}