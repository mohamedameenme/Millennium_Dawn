# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Eritrean border conflict with Djibouti
Eritrea_Djibouti_Border_Conflict = {
	enable = {
		original_tag = ERI
		country_exists = DJI
		has_government = nationalist
		has_opinion_modifier = ERI_Border_Disputes
	}
	abort = {
		OR = {
			NOT = { country_exists = DJI }
			NOT = { has_government = nationalist }
			NOT = { has_opinion_modifier = ERI_Border_Disputes }
		}

	}
	
	ai_strategy = {
		type = antagonize
		id = "DJI"
		value = 50
	}
	ai_strategy = {
		type = contain
		id = "DJI"
		value = 100
	}
	ai_strategy = {
		type = conquer
		id = "DJI"
		value = 50
	}
}

#Eritrea-Ethiopia border conflict
Eritrea_Ethiopia_Border_Conflict = {
	enable = {
		original_tag = ERI
		country_exists = ETH
		has_government = nationalist
		has_opinion_modifier = ERI_Border_Disputes
	}
	abort = {
		OR = {
			NOT = { country_exists = ETH }
			NOT = { has_government = nationalist }
			NOT = { has_opinion_modifier = ERI_Border_Disputes }
		}
	}
	
	ai_strategy = {
		type = antagonize
		id = "ETH"
		value = 50
	}
	ai_strategy = {
		type = contain
		id = "ETH"
		value = 100
	}
	ai_strategy = {
		type = conquer
		id = "ETH"
		value = 50
	}
}

#Eritrea supplies weapons to Al-Shabaab
Eritrea_supply_SHB = {

	enable = {
		original_tag = ERI
	}
	
	abort = {
		has_war_with = SHB
		is_in_faction_with = SOM
	}
	
	ai_strategy = {
		type = befriend 
		id = "SHB"
		value = 100
	}
	
	ai_strategy = {
		type = support 
		id = "SHB"
		value = 100
	}
	
	ai_strategy = {
		type = antagonize
		id = "SOM"
		value = 50
	}
	
	ai_strategy = {
		type = contain
		id = "SOM"
		value = 100
	}
}