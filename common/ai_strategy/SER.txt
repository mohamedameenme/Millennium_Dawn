# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

Serbia_Kosovo_Relations = {
	enable = {
		tag = SER
		country_exists = KOS
	}
	abort = {
		OR = {
			NOT = { country_exists = KOS }
			KOS = { is_subject_of = SER }
		}
	}

	ai_strategy = {
		type = antagonize
		id = "KOS"
		value = 50
	}
	ai_strategy = {
		type = conquer
		id = "KOS"
		value = 50
	}
}