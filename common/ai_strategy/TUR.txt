# air_factory_balance
# alliance
# antagonize
# area_priority
# befriend
# build_army
# build_building
# build_ship
# conquer
# contain
# equipment_production_factor
# equipment_stockpile
# garrison
# ignore
# influence
# invade
# pp_spend_priority
# protect
# research_tech
# role_ratio
# send_volunteers_desire
# support
# template_prio
# template_xp_reserve 
# unit_ratio

#Turkey backs National Salvation Government in Libya (Muslim Brotherhood, weak)
Turkey_Support_GNC = {
	
	enable = {
		original_tag = TUR
	}
	
	abort = {
		GNC = { 
			OR = { 
				war_with_us = yes 
				is_bad_salafist = yes
				in_different_faction = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "GNC"
		value = 50
	}
	
	ai_strategy = {
		type = protect 
		id = "GNC"
		value = 50
	}
	
	ai_strategy = {
		type = influence
		id = "GNC"
		value = 50
	}
	
	ai_strategy = {
		type = support
		id = "GNC"
		value = 50
	}
	
}

#Turkey supports FSA
Turkey_Intervene_SYR = {

	enable = {
		original_tag = TUR
	}
	
	abort = {
		FSA = { 
			OR = { 
				war_with_us = yes 
				is_bad_salafist = yes
			}
		}
	}
	
	ai_strategy = {
		type = befriend 
		id = "FSA"
		value = 100
	}
	
	ai_strategy = {
		type = protect 
		id = "FSA"
		value = 100
	}
	
	ai_strategy = {
		type = support
		id = "FSA"
		value = 150
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "FSA"
		value = 200
	}
	
}

#Turkey supports ALA
Turkey_Intervene_SYR_2 = {

	enable = {
		original_tag = TUR
		158 = {
			NOT = { is_core_of = ALA }
			NOT = { is_claimed_by = ALA }
		}
	}
	
	abort = {
		ALA = { 
			OR = { 
				war_with_us = yes 
				is_bad_salafist = yes
			}
		}
	}
	
	ai_strategy = {
		type = support
		id = "ALA"
		value = 200
	}
	
	ai_strategy = {
		type = send_volunteers_desire
		id = "ALA"
		value = 200
	}
	
}