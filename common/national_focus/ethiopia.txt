focus_tree = {
	id = ethiopia
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = ETH
		}
	}
	default = yes
	shared_focus = ETH_invite_foreign_investors
	shared_focus = ETH_international_diplomacy
	shared_focus = ETH_defense_of_ethiopia
	shared_focus = ETH_the_future_of_ethiopia
	shared_focus = ETH_legacy_of_abyssinia
	shared_focus = ETH_strengthen_the_republic
	shared_focus = ETH_declare_mengistu_innocent
}

### Just for setting up the shared focus tree, to later load the Ethiopian-Eritrean tree
