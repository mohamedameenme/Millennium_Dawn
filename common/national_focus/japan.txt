focus_tree = {
	
	id = japan_focus
	
	country = {
		factor = 0
		
		modifier = {
			add = 20
			original_tag = JAP
		}
	}
	
	#Economic Reform
	focus = {
		id = JAP_economic_reform
		icon = zaibatsu
		
		x = 2
		y = 0
		
		cost = 10
		
		prerequisite = { }
		mutually_exclusive = { }
		available = { }
		historical_ai = { }
		select_effect = { }
		bypass = { }
		cancel = { }

		cancel_if_invalid = no
		continue_if_invalid = yes
		available_if_capitulated = no
		
		complete_tooltip = { }
		completion_reward = {
			if = {
				limit = { has_idea = depression }
				swap_ideas = { remove_idea = depression add_idea = recession }
			}
			if = {
				limit = { has_idea = recession }
				swap_ideas = { remove_idea = recession add_idea = stagnation }
			}
			if = {
				limit = { has_idea = stagnation }
				swap_ideas = { remove_idea = stagnation add_idea = stable_growth }
			}
			if = {
				limit = { has_idea = stable_growth }
				swap_ideas = { remove_idea = stable_growth add_idea = fast_growth }
			}
			if = {
				limit = { has_idea = fast_growth }
				swap_ideas = { remove_idea = fast_growth add_idea = economic_boom }
			}
			if = {
				limit = { has_idea = economic_boom }
				add_political_power = 100 #if desired economic level is reached before PP is rewarded instead
				add_stability = 0.05
			}
		}

		ai_will_do = {
			base = 10
		}
	}
	focus = {
		id = JAP_invest_in_toyota
		icon = toyota
		
		x = -1
		y = 1
		relative_position_id = JAP_economic_reform
		
		cost = 12
		
		prerequisite = { focus = JAP_economic_reform }
		mutually_exclusive = { }
		available = { }
		historical_ai = { }
		select_effect = { }
		bypass = { }
		cancel = { }

		cancel_if_invalid = no
		continue_if_invalid = yes
		available_if_capitulated = no
		
		complete_tooltip = { }
		completion_reward = {
			custom_effect_tooltip = JAP_int_investment_5_bil_tt
			add_to_variable = { int_investments = 5.0 }
			613 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
			}
		}

		ai_will_do = {
			base = 10
		}
	}
	focus = {
		id = JAP_expand_the_automotive_industry
		icon = production
		
		x = -2
		y = 2
		relative_position_id = JAP_economic_reform
		
		cost = 10
		
		prerequisite = { focus = JAP_invest_in_toyota }
		mutually_exclusive = { }
		available = { }
		historical_ai = { }
		select_effect = { }
		bypass = { }
		cancel = { }

		cancel_if_invalid = no
		continue_if_invalid = yes
		available_if_capitulated = no
		
		complete_tooltip = { }
		completion_reward = { 
			615 = {
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}

		ai_will_do = {
			base = 10
		}
	}
	focus = {
		id = JAP_reform_the_economic_ministry
		icon = japanese_imperialism
		
		x = 0
		y = 2
		relative_position_id = JAP_economic_reform
		
		cost = 7
		
		prerequisite = { focus = JAP_invest_in_toyota }
		mutually_exclusive = { }
		available = { }
		historical_ai = { }
		select_effect = { }
		bypass = { }
		cancel = { }

		cancel_if_invalid = no
		continue_if_invalid = yes
		available_if_capitulated = no
		
		complete_tooltip = { }
		completion_reward = { 
			add_ideas = {
				JAP_reformed_economic_ministry
			}
		}

		ai_will_do = {
			base = 10
		}
	}
	focus = {
		id = JAP_investments_in_south_east_asia
		icon = south_east_asian_offensive
		
		x = -1
		y = 3
		relative_position_id = JAP_economic_reform
		
		cost = 12
		
		prerequisite = { focus = JAP_reform_the_economic_ministry }
		mutually_exclusive = { }
		available = { }
		historical_ai = { }
		select_effect = { }
		bypass = { }
		cancel = { }

		cancel_if_invalid = no
		continue_if_invalid = yes
		available_if_capitulated = no
		
		complete_tooltip = { }
		completion_reward = { 
			add_political_power = 50
			custom_effect_tooltip = JAP_int_investment_5_bil_tt
			add_to_variable = { int_investments = 5.0 }
		}

		ai_will_do = {
			base = 10
		}
	}
	focus = {
		id = JAP_support_stagnant_businesses
		icon = better_money
		
		x = 1
		y = 3
		relative_position_id = JAP_economic_reform
		
		cost = 12
		
		prerequisite = { focus = JAP_reform_the_economic_ministry }
		mutually_exclusive = { }
		available = { }
		historical_ai = { }
		select_effect = { }
		bypass = { }
		cancel = { }

		cancel_if_invalid = no
		continue_if_invalid = yes
		available_if_capitulated = no
		
		complete_tooltip = { }
		completion_reward = {
			custom_effect_tooltip = JAP_treasury_10_bil_tt
			subtract_from_variable = { treasury = 10 }
			add_ideas = {
				JAP_support_business
			}
		}

		ai_will_do = {
			base = 10
		}
	}
	focus = {
		id = JAP_support_agriculture_abroad
		icon = manchurian_project
		
		x = -2
		y = 4
		relative_position_id = JAP_economic_reform
		
		cost = 12
		
		prerequisite = { focus = JAP_investments_in_south_east_asia }
		mutually_exclusive = { }
		available = { }
		historical_ai = { }
		select_effect = { }
		bypass = { }
		cancel = { }

		cancel_if_invalid = no
		continue_if_invalid = yes
		available_if_capitulated = no
		
		complete_tooltip = { }
		completion_reward = {
			add_political_power = 50
			custom_effect_tooltip = JAP_int_investment_5_bil_tt
			add_to_variable = { int_investments = 5.0 }
		}

		ai_will_do = {
			base = 10
		}
	}
	focus = {
		id = JAP_cultural_revitalization
		icon = spiritual_mobilization
		
		x = 0
		y = 4
		relative_position_id = JAP_economic_reform
		
		cost = 10
		
		prerequisite = { focus = JAP_investments_in_south_east_asia }
		mutually_exclusive = { }
		available = { }
		historical_ai = { }
		select_effect = { }
		bypass = { }
		cancel = { }

		cancel_if_invalid = no
		continue_if_invalid = yes
		available_if_capitulated = no
		
		complete_tooltip = { }
		completion_reward = { 
			add_political_power = 100
			add_stability = 0.05
			add_popularity = {
				ideology = nationalist
				popularity = 0.05
			}
			custom_effect_tooltip = JAP_add_pop_Monarchist_5_TT
			add_to_variable = {
				var = Monarchist
				value = 0.05
			}
			recalculate_party = yes
		}

		ai_will_do = {
			base = 10
		}
	}
	focus = {
		id = JAP_devalue_personal_savings_accounts
		icon = money
		
		x = -2
		y = 5
		relative_position_id = JAP_economic_reform
		
		cost = 12
		
		prerequisite = { focus = JAP_support_agriculture_abroad }
		mutually_exclusive = { }
		available = { }
		historical_ai = { }
		select_effect = { }
		bypass = { }
		cancel = { }

		cancel_if_invalid = no
		continue_if_invalid = yes
		available_if_capitulated = no
		
		complete_tooltip = { }
		completion_reward = {
			add_timed_idea = {
				idea = JAP_devalued_personal_savings
				days = 240
			}
		}

		ai_will_do = {
			base = 10
		}
	}
	focus = {
		id = JAP_invite_asian_scientists
		icon = research
		
		x = 0
		y = 5
		relative_position_id = JAP_economic_reform
		
		cost = 10
		
		prerequisite = { focus = JAP_cultural_revitalization }
		mutually_exclusive = { }
		available = { }
		historical_ai = { }
		select_effect = { }
		bypass = { }
		cancel = { }

		cancel_if_invalid = no
		continue_if_invalid = yes
		available_if_capitulated = no
		
		complete_tooltip = { }
		completion_reward = { 
			add_research_slot = 1
		}

		ai_will_do = {
			base = 10
		}
	}
	focus = {
		id = JAP_break_the_deflation
		icon = money_on_fire
		
		x = -2
		y = 6
		relative_position_id = JAP_economic_reform
		
		cost = 12
		
		prerequisite = { focus = JAP_devalue_personal_savings_accounts }
		mutually_exclusive = { }
		available = { }
		historical_ai = { }
		select_effect = { }
		bypass = { }
		cancel = { }

		cancel_if_invalid = no
		continue_if_invalid = yes
		available_if_capitulated = no
		
		complete_tooltip = { }
		completion_reward = { 
			remove_ideas = JAP_deflation
		}

		ai_will_do = {
			base = 10
		}
	}
	
	#Honshu
	focus = {
		id = JAP_develop_honshu
		icon = industry
		x = 5
		y = 1
		cost = 15
		
		prerequisite = { focus = JAP_economic_reform }
		
		available = {
			has_full_control_of_state = 615
			has_full_control_of_state = 612
			NOT = { has_idea = JAP_deflation }
		}
		
		completion_reward = {
			615 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = industrial_complex
					level = 2
					instant_build = yes
				}
			}
			612 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = industrial_complex
					level = 2
					instant_build = yes
				}
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	focus = {
		id = JAP_honshu_steel
		icon = steel_production
		x = -1
		y = 1
		relative_position_id = JAP_develop_honshu
		cost = 10
		
		available = {
			has_full_control_of_state = 611
			has_full_control_of_state = 612
			has_full_control_of_state = 613
			has_full_control_of_state = 615
			has_full_control_of_state = 616
		}
		
		prerequisite = {
			focus = JAP_develop_honshu
		}
		
		completion_reward = {
			611 = { add_resource = { type = steel amount = 1 } }
			612 = { add_resource = { type = steel amount = 1 } }
			613 = { add_resource = { type = steel amount = 1 } }
			616 = { add_resource = { type = steel amount = 1 } }
			615 = { add_resource = { type = steel amount = 3 } }
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_develop_kyushu
		icon = construction
		x = 0
		y = 2
		relative_position_id = JAP_develop_honshu
		cost = 15
		
		available = { has_full_control_of_state = 609 }
		
		prerequisite = {
			focus = JAP_develop_honshu
		}
		
		completion_reward = {
			609 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_develop_shikoku
		icon = construction2
		x = -1
		y = 3
		relative_position_id = JAP_develop_honshu
		cost = 15
		
		prerequisite = {
			focus = JAP_develop_kyushu
		}
		
		available = {
			has_full_control_of_state = 610
		}
		
		completion_reward = {
			610 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_oil_production
		icon = oil_production
		x = 1
		y = 3
		relative_position_id = JAP_develop_honshu
		cost = 10
		
		available = { has_full_control_of_state = 615 }
		
		prerequisite = {
			focus = JAP_develop_kyushu
		}
		
		completion_reward = {
			615 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = fuel_silo
					level = 1
					instant_build = yes
				}
				add_building_construction = {
					type = synthetic_refinery
					level = 1
					instant_build = yes
				}
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_develop_hokkaido
		icon = construction3
		x = -1
		y = 4
		relative_position_id = JAP_develop_honshu
		cost = 15
		
		available = { has_full_control_of_state = 617 }
		
		prerequisite = {
			focus = JAP_develop_shikoku
		}
		
		completion_reward = {
			617 = {
				add_extra_state_shared_building_slots = 2
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
				add_building_construction = {
					type = arms_factory
					level = 1
					instant_build = yes
				}
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_rare_metal_production
		icon = production2
		x = 1
		y = 4
		relative_position_id = JAP_develop_honshu
		cost = 10
		
		prerequisite = {
			focus = JAP_oil_production
		}
		
		completion_reward = {
			random_owned_state = { add_resource = { type = steel amount = 2 } }
			random_owned_state = { add_resource = { type = steel amount = 2 } }
			random_owned_state = { add_resource = { type = aluminium amount = 2 } }
			random_owned_state = { add_resource = { type = aluminium amount = 2 } }
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_extra_research_slot
		icon = research
		x = 0
		y = 5
		relative_position_id = JAP_develop_honshu
		cost = 10
		
		prerequisite = {
			focus = JAP_develop_hokkaido 
		}
		prerequisite = {
			focus = JAP_rare_metal_production
		}
		
		completion_reward = {
			add_research_slot = 1
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	###
	
	#National Renewal
	focus = {
		id = JAP_national_renewal
		icon = national_unity
		x = 8
		y = 1
		cost = 10
		
		prerequisite = { focus = JAP_economic_reform }
		
		available = {
			NOT = { has_idea = JAP_deflation }
		}
		
		completion_reward = {
			add_political_power = 50
			add_stability = 0.05
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_path_of_peace
		icon = demand_territory2
		x = -1
		y = 1
		relative_position_id = JAP_national_renewal
		cost = 10
		
		prerequisite = {
			focus = JAP_national_renewal
		}
		
		mutually_exclusive = {
			focus = JAP_path_of_war
		}
		
		completion_reward = {
			add_stability = 0.05
			add_war_support = -0.05
			add_popularity = {
				ideology = democratic
				popularity = 0.05
			}
			custom_effect_tooltip = JAP_add_pop_western_5_TT
			recalculate_party = yes
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_path_of_war
		icon = the_zero
		x = 1
		y = 1
		relative_position_id = JAP_national_renewal
		cost = 10
		
		prerequisite = {
			focus = JAP_national_renewal
		}
		
		mutually_exclusive = {
			focus = JAP_path_of_peace
		}
		
		completion_reward = {
			add_stability = -0.05
			add_war_support = 0.05
			add_popularity = {
				ideology = nationalist
				popularity = 0.05
			}
			custom_effect_tooltip = JAP_add_pop_Nat_Autocracy_5_TT
			add_to_variable = {
				var = Nat_Autocracy
				value = 0.05
			}
			recalculate_party = yes
		}
		
		ai_will_do = {
			factor = 10
			modifier = {
				is_historical_focus_on = yes
				factor = 0
			}
		}
	}
	focus = {
		id = JAP_national_industrial_project
		icon = industry_military
		x = -1
		y = 2
		relative_position_id = JAP_national_renewal
		cost = 10
		
		prerequisite = {
			focus = JAP_path_of_peace
		}
		
		completion_reward = {
			add_ideas = idea_focus_industrial_development_program
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_recruitment_programs
		icon = propaganda
		x = 1
		y = 2
		relative_position_id = JAP_national_renewal
		cost = 10
		
		prerequisite = {
			focus = JAP_path_of_war
		}
		
		completion_reward = {
			add_ideas = militarism
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_political_renewal
		icon = demand_territory
		x = 0
		y = 3
		relative_position_id = JAP_national_renewal
		cost = 10
		
		prerequisite = {
			focus = JAP_national_industrial_project
			focus = JAP_recruitment_programs
		}
		
		completion_reward = {
			add_political_power = 150
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_adapt_article_9
		icon = renounce_treaty
		x = 1
		y = 4
		relative_position_id = JAP_national_renewal
		cost = 10
		
		prerequisite = {
			focus = JAP_political_renewal
			focus = JAP_recruitment_programs
		}
		
		available = {
			OR = {
				has_completed_focus = JAP_recruitment_programs
				threat > 0.2
			}
		}
		
		bypass = {
			NOT = { has_idea = article_nine_jap }
		}
		
		completion_reward = {
			add_named_threat = {
				threat = 1
				name = "Article 9 Revoked"
			}
			remove_ideas = article_nine_jap
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_ideological_debate
		icon = ultimatum2
		x = 0
		y = 5
		relative_position_id = JAP_national_renewal
		cost = 10
		
		prerequisite = {
			focus = JAP_political_renewal
		}
		
		completion_reward = {
			add_political_power = 150
			add_timed_idea = {
				idea = JAP_ideological_debate
				days = 365
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	focus = {
		id = JAP_bushido
		icon = japanese_imperial_glory
		x = 14
		y = 0
		cost = 10
		
		completion_reward = {
			add_tech_bonus = {
				name = land_doc_bonus
				bonus = 0.15
				uses = 1
				category = land_doctrine
			}
			add_tech_bonus = {
				name = naval_doc_bonus
				bonus = 0.15
				uses = 1
				category = Cat_naval_doctrine
			}
			add_tech_bonus = {
				name = air_doc_bonus
				bonus = 0.15
				uses = 1
				category = air_doctrine
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	
	focus = {
		id = JAP_continental_campaigns
		icon = infantry_axis
		x = -2
		y = 1
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_bushido
		}
		
		completion_reward = {
			army_experience = 10
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_ocean_campaigns
		icon = navy2
		x = 2
		y = 1
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_bushido
		}
		
		completion_reward = {
			navy_experience = 10
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_air_force_focus
		icon = air_force
		x = 5
		y = 1
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_bushido
		}
		
		completion_reward = {
			air_experience = 10
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	
	
	
	
	focus = {
		id = JAP_artillery_training
		icon = artillery2
		x = -3
		y = 2
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_continental_campaigns
		}
		
		completion_reward = {
			add_tech_bonus = {
				name = artillery_bonus
				bonus = 0.5
				uses = 1
				category = CAT_ARTY
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	focus = {
		id = JAP_officer_school
		icon = conscription
		x = -1
		y = 2
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_continental_campaigns
		}
		
		completion_reward = {
			add_ideas = {
				JAP_officer_schools
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	focus = {
		id = JAP_destroyer_program
		icon = anti_submarine
		x = 1
		y = 2
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_ocean_campaigns
		}
		
		completion_reward = {
			add_tech_bonus = {
				name = destroyer_bonus
				bonus = 0.25
				uses = 2
				category = Cat_DESTROYER
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	focus = {
		id = JAP_cruiser_program
		icon = cruisers
		x = 3
		y = 2
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_ocean_campaigns
		}
		
		completion_reward = {
			add_tech_bonus = {
				name = cruiser_bonus
				bonus = 0.3
				uses = 2
				category = Cat_N_CRUISER
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	focus = {
		id = JAP_light_planes
		icon = fighters
		x = 5
		y = 2
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_air_force_focus
		}
		
		completion_reward = {
			add_ideas = idea_focus_generic_fighter_focus
			add_tech_bonus = {
				name = fighter_bonus
				bonus = 0.45
				uses = 2
				category = CAT_FIXED_WING
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	
	
	
	focus = {
		id = JAP_infantry_training
		icon = small_arms2
		x = -2
		y = 3
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_artillery_training
		}
		prerequisite = {
			focus = JAP_officer_school
		}
		
		completion_reward = {
			army_experience = 20
			add_tech_bonus = {
				uses = 1
				bonus = 0.75
				category = CAT_INF_WEP
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	focus = {
		id = JAP_submarine_program
		icon = submarines
		x = 1
		y = 3
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_destroyer_program
		}
		
		completion_reward = {
			add_tech_bonus = {
				name = submarine_bonus
				bonus = 0.25
				uses = 3
				category = Cat_SUB
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	focus = {
		id = JAP_carrier_program
		icon = carriers
		x = 3
		y = 3
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_cruiser_program
		}
		
		completion_reward = {
			add_tech_bonus = {
				name = carrier_bonus
				bonus = 0.4
				uses = 2
				category = Cat_CARRIER
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	focus = {
		id = JAP_heavy_planes
		icon = bombers
		x = 5
		y = 3
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_light_planes
		}
		
		completion_reward = {
			add_tech_bonus = {
				name = fighter_bonus
				bonus = 0.35
				uses = 2
				category = Cat_STR_BOMBER
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	
	
	
	focus = {
		id = JAP_tank_program
		icon = tanks3
		x = -3
		y = 4
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_infantry_training
		}
		
		completion_reward = {
			add_tech_bonus = {
				name = armor_bonus
				bonus = 0.2
				uses = 2
				category = Cat_ARMOR
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	focus = {
		id = JAP_army_exercises
		icon = war_against_japan
		x = -1
		y = 4
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_infantry_training
		}
		
		completion_reward = {
			army_experience = 25
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	focus = {
		id = JAP_naval_production
		icon = navy
		x = 1
		y = 4
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_submarine_program
			focus = JAP_carrier_program
		}
		
		completion_reward = {
			navy_experience = 50
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	focus = {
		id = JAP_naval_invasion_protocols
		icon = amphibious_assault
		x = 3
		y = 4
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_submarine_program
			focus = JAP_carrier_program
		}
		
		completion_reward = {
			navy_experience = 50
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	focus = {
		id = JAP_air_base_project
		icon = air_production
		x = 6
		y = 4
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_light_planes
		}
		prerequisite = {
			focus = JAP_northern_expressways
		}
		
		completion_reward = {
			random_owned_state = {
				add_building_construction = {
					type = air_base
					level = 2
					instant_build = yes
				}
			}
			random_owned_state = {
				add_building_construction = {
					type = air_base
					level = 2
					instant_build = yes
				}
			}
			random_owned_state = {
				add_building_construction = {
					type = air_base
					level = 2
					instant_build = yes
				}
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	
	
	focus = {
		id = JAP_army_doctrine_school
		icon = army_doctrine
		x = -1
		y = 5
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_army_exercises
		}
		
		completion_reward = {
			add_tech_bonus = {
				name = land_doc_bonus
				bonus = 0.5
				uses = 1
				category = land_doctrine
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	focus = {
		id = JAP_navy_doctrine_school
		icon = naval_doctrine
		x = 1
		y = 5
		relative_position_id = JAP_bushido
		cost = 10
		
		prerequisite = {
			focus = JAP_naval_production
		}
		
		completion_reward = {
			add_tech_bonus = {
				name = naval_doc_bonus
				bonus = 0.5
				uses = 1
				category = Cat_naval_doctrine
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	#Nishiseto
	focus = {
		id = JAP_finish_nishiseto
		icon = infrastructure
		x = 22
		y = 0
		cost = 12
		
		available = {
			has_full_control_of_state = 610
			has_full_control_of_state = 612
		}
		
		completion_reward = {
			610 = {
				add_building_construction = {
					type = infrastructure
					level = 2
					instant_build = yes
				}
			}
			612 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_southern_expressways
		icon = infrastructure
		x = -1
		y = 1
		relative_position_id = JAP_finish_nishiseto
		cost = 10
		
		available = {
			has_full_control_of_state = 611
			has_full_control_of_state = 612
		}
		
		prerequisite = {
			focus = JAP_finish_nishiseto
		}
		
		completion_reward = {
			612 = { add_building_construction = { type = infrastructure level = 1 instant_build = yes } }
			611 = { add_building_construction = { type = infrastructure level = 1 instant_build = yes } }
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_new_trade_policy
		icon = trade
		x = 2
		y = 1
		relative_position_id = JAP_finish_nishiseto
		cost = 10
		
		prerequisite = {
			focus = JAP_finish_nishiseto
		}
		
		completion_reward = {
			every_country = {
				limit = {
					NOT = { 
						original_tag = USA
						original_tag = CHI
						original_tag = NKO
						original_tag = KOR
					}
					NOT = { has_war_with = ROOT }
					has_opinion = {
						target = ROOT
						value > 5
					}
				}
				add_opinion_modifier = {
					target = JAP
					modifier = improve_trade
				}
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_northern_expressways
		icon = infrastructure
		x = -1
		y = 2
		relative_position_id = JAP_finish_nishiseto
		cost = 10
		
		prerequisite = {
			focus = JAP_southern_expressways
		}
		
		available = {
			has_full_control_of_state = 615
			has_full_control_of_state = 616
			has_full_control_of_state = 617
		}
		
		completion_reward = {
			615 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			616 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
			617 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_trade_with_america
		icon = align_to_america
		x = 1
		y = 2
		relative_position_id = JAP_finish_nishiseto
		cost = 10
		
		prerequisite = {
			focus = JAP_new_trade_policy
		}
		
		mutually_exclusive = {
			focus = JAP_trade_with_china
		}
		
		completion_reward = {
			USA = {
				add_opinion_modifier = {
					target = JAP
					modifier = improve_trade
				}
				reverse_add_opinion_modifier = {
					target = JAP
					modifier = improve_trade
				}
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_trade_with_china
		icon = align_to_china
		x = 3
		y = 2
		relative_position_id = JAP_finish_nishiseto
		cost = 10
		
		prerequisite = {
			focus = JAP_new_trade_policy
		}
		
		mutually_exclusive = {
			focus = JAP_trade_with_america
		}
		
		completion_reward = {
			CHI = {
				add_opinion_modifier = {
					target = JAP
					modifier = improve_trade
				}
				reverse_add_opinion_modifier = {
					target = JAP
					modifier = improve_trade
				}
			}
		}
		
		ai_will_do = {
			factor = 10
			modifier = {
				is_historical_focus_on = yes
				factor = 0
			}
		}
	}
	focus = {
		id = JAP_trade_with_south_korea
		icon = align_to_south_korea
		x = 1
		y = 3
		relative_position_id = JAP_finish_nishiseto
		cost = 10
		
		prerequisite = {
			focus = JAP_trade_with_america
		}
		
		available = { NOT = { has_war_with = KOR } }
		
		completion_reward = {
			KOR = {
				add_opinion_modifier = { target = JAP modifier = improve_trade }
				reverse_add_opinion_modifier = { target = JAP modifier = improve_trade }
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	focus = {
		id = JAP_trade_with_north_korea
		icon = align_to_north_korea
		x = 3
		y = 3
		relative_position_id = JAP_finish_nishiseto
		cost = 10
		
		prerequisite = {
			focus = JAP_trade_with_china
		}
		
		available = { NOT = { has_war_with = NKO } }
		
		completion_reward = {
			NKO = {
				add_opinion_modifier = { target = JAP modifier = improve_trade }
				reverse_add_opinion_modifier = { target = JAP modifier = improve_trade }
			}
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
	
	#THE FUTURE OF JAPAN
	
	focus = {
		id = JAP_the_future_of_japan
		icon = demand_territory
		x = 14
		y = 7
		cost = 10
		
		prerequisite = {
			focus = JAP_ideological_debate
		}
		prerequisite = {
			focus = JAP_army_doctrine_school
			focus = JAP_navy_doctrine_school
		}
		
		completion_reward = {
			add_political_power = 120
		}
		
		ai_will_do = {
			factor = 10
		}
	}
	
}