﻿###########################
# News Events
# 1-x by HansNery
###########################

add_namespace = news

# Free Trade Agreement Signed
news_event = {
	id = news.1
	title = news.1.t
	desc = news.1.d
	picture = GFX_trade_agreement
	
	major = yes
	
	is_triggered_only = yes
	
	option = {
		name = news.1.a
		log = "[GetDateText]: [This.GetName]: news.1.a executed"
	}
}

# Military Cooperation Treaty Signed
news_event = {
	id = news.2
	title = news.2.t
	desc = news.2.d
	picture = GFX_trade_agreement
	
	major = yes
	
	is_triggered_only = yes
	
	option = {
		name = news.2.a
		log = "[GetDateText]: [This.GetName]: news.2.a executed"
	}
}

#USS Cole News Event
news_event = {
	id = news.3
	title = news.3.t
	desc = news.3.d
	picture = GFX_news_cole_attack
	
	major = yes
	
	is_triggered_only = yes
	
	option = {
		name = news.3.a
		log = "[GetDateText]: [This.GetName]: news.3.a executed"
		trigger = {
			original_tag = USA
		}
	}
	
	option = {
		name = news.3.b
		log = "[GetDateText]: [This.GetName]: news.3.b executed"
		trigger = {
			NOT = { original_tag = USA }
			NOT = { original_tag = NKO }
			NOT = { original_tag = SUD }
			NOT = { has_war_with = USA }
		}
	}
	
	option = {
		name = news.3.c
		log = "[GetDateText]: [This.GetName]: news.3.c executed"
		trigger = {
			OR = {
				original_tag = NKO
				has_war_with = USA
			}
		}
	}
	
	option = {
		name = news.3.e
		log = "[GetDateText]: [This.GetName]: news.3.e executed"
		trigger = {
			original_tag = SUD
		}
	}
}

#[Country] leaves the EU!
news_event = {
	id = news.4
	title = news.4.t
	desc = news.4.d
	picture = GFX_eu
	
	major = yes
	is_triggered_only = yes
	
	option = {
		name = news.4.o1
		log = "[GetDateText]: [This.GetName]: news.4.a executed"
	}
}