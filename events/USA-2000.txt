﻿add_namespace = usa

#USS Cole Attacks
country_event = {
	id = usa.2
	title = usa.2.t
	desc = usa.2.d
	picture = GFX_report_event_cole_attack
	
	is_triggered_only = yes
	
	immediate = {
		set_country_flag = usa_uss_cole_attack
	}
	
	option = {
		name = usa.2.o1
		log = "[GetDateText]: [This.GetName]: usa.2.o1 executed"
		news_event = {
			hours = 6
			id = usa_news.18
		}
		navy_experience = 2
		add_political_power = -10
		add_opinion_modifier = {
			target = SUD
			modifier = recent_actions_negative
		}
	}
}

#Southern Illinois Incident
country_event = {
	id = usa.3
	title = usa.3.t
	desc = usa.3.d
	picture = GFX_report_event_ufo
	
	is_triggered_only = yes
	
	trigger = {
		has_full_control_of_state = 778	#Illinois
	}
	
	option = {
		name = usa.3.o1
		log = "[GetDateText]: [This.GetName]: usa.3.o1 executed"
		add_political_power = -1
	}
}

#Microsoft acquires Visio
country_event = {
	id = usa.4
	title = usa.4.t
	desc = usa.4.d
	picture = GFX_report_event_stock_market
	
	is_triggered_only = yes
	
	trigger = {
		NOT = { has_government = communism }
	}
	
	option = {
		name = usa.4.o1
		log = "[GetDateText]: [This.GetName]: usa.4.o1 executed"
		add_political_power = 5
	}
	
	option = {
		name = usa.4.o2
		log = "[GetDateText]: [This.GetName]: usa.4.o2 executed"
		add_stability = -0.02
		add_to_variable = {
			var = Communist-State
			value = 0.02
		}
		recalculate_party = yes
		custom_effect_tooltip = usa.4.o2_tt
	}
}

#America Online merges with Time Warner
country_event = {
	id = usa.5
	title = usa.5.t
	desc = usa.5.d
	picture = GFX_report_event_stock_market
	
	is_triggered_only = yes
	
	trigger = {
		NOT = { has_government = communism }
	}
	
	option = {
		name = usa.5.o1
		log = "[GetDateText]: [This.GetName]: usa.5.o1 executed"
		add_political_power = 30
	}
	
	option = {
		name = usa.5.o2
		log = "[GetDateText]: [This.GetName]: usa.5.o2 executed"
		add_stability = -0.03
		add_to_variable = {
			var = Communist-State
			value = 0.06
		}
		recalculate_party = yes
		custom_effect_tooltip = usa.5_tt
	}
}

#January 2000 North American Blizzard
country_event = {
	id = usa.6
	title = usa.6.t
	desc = usa.6.d
	picture = GFX_report_event_blizzard
	
	is_triggered_only = yes
	
	option = {
		name = usa.6.o1
		log = "[GetDateText]: [This.GetName]: usa.6.o1 executed"
		add_political_power = -15
		add_stability = -0.04
		ai_chance = { factor = 0 }
	}
	
	option = {
		name = usa.6.o2
		log = "[GetDateText]: [This.GetName]: usa.6.o2 executed"
		add_political_power = -60
		add_stability = -0.01
		ai_chance = { factor = 100 }
	}
}

#Elian Gonzalez Affair
country_event = {
	id = usa.7
	title = usa.7.t
	desc = usa.7.d
	picture = GFX_report_event_elian_gonzalez
	
	is_triggered_only = yes
	
	trigger = {
		country_exists = CUB
		NOT = { has_government = communism }
		CUB = { has_government = communism }
	}
	
	option = {	#Send the boy back
		name = usa.7.o1
		log = "[GetDateText]: [This.GetName]: usa.7.o1 executed"
		add_political_power = 25
		
		hidden_effect = {
			country_event = {
				id = usa.51
				days = 102
			}
		}
		
		ai_chance = {
			factor = 10
		}
		
		set_country_flag = usa_elian_gonzalez_sent_to_cuba
	}
	
	option = {	#Keep him here
		name = usa.7.o2
		log = "[GetDateText]: [This.GetName]: usa.7.o2 executed"
		add_stability = 0.01
		CUB = {
			add_opinion_modifier = {
				target = USA
				modifier = recent_actions_negative
			}
		}
		set_country_flag = usa_elian_gonzalez_kept_in_the_states
		add_to_variable = {
			var = liberalism
			value = 0.02
		}
		add_to_variable = {
			var = Communist-State
			value = 0.03
		}
		recalculate_party = yes
		custom_effect_tooltip = usa.7.o2_tt	
		add_named_threat = { 
			threat = 0.3
			name = usa.7.t 
		}
		ai_chance = {
			factor = 5
			modifier = {
				factor = 0
				is_historical_focus_on = yes
			}
		}
	}
}

#Greeneville sinks Ehime-Maru
country_event = {
	id = usa.8
	title = usa.8.t
	desc = usa.8.d
	picture = GFX_report_event_submarine
	
	is_triggered_only = yes
	
	option = {	#Apologize
		name = usa.8.o1
		log = "[GetDateText]: [This.GetName]: usa.8.o1 executed"	
		add_stability = -0.01
	}
	
	option = {	#It's their fault
		name = usa.8.o2
		log = "[GetDateText]: [This.GetName]: usa.8.o2 executed"
		add_stability = 0.01
		add_named_threat = { 
			threat = 0.03
			name = usa.8.t 
		}
		JAP = {
			add_opinion_modifier = {
				target = USA
				modifier = recent_actions_negative
			}
		}
		add_to_variable = {
			var = Nat_Populism
			value = 0.03
		}
		custom_effect_tooltip = usa.8.o2_tt
	}
}

#Alaska Airlines Flight 261
country_event = {
	id = usa.9
	title = usa.9.t
	desc = usa.9.d
	picture = GFX_report_event_plane_crash
	
	is_triggered_only = yes
	
	option = {
		name = usa.9.o1
		log = "[GetDateText]: [This.GetName]: usa.9.01 executed"
		add_political_power = -20
	}
}

#End of the Peanuts
country_event = {
	id = usa.10
	title = usa.10.t
	desc = usa.10.d
	picture = GFX_report_event_comic_artist
	
	is_triggered_only = yes
	
	option = {
		name = usa.10.o1
		log = "[GetDateText]: [This.GetName]: usa.10.o1 executed"
		add_political_power = 5
	}
}

#Shooting of Kayla Rolland
country_event = {
	id = usa.11
	title = usa.11.t
	desc = usa.11.d
	picture = GFX_report_event_handgun
	
	is_triggered_only = yes
	
	trigger = {
		has_full_control_of_state = 777 #Michigan
	}
	
	option = {
		name = usa.11.o1
		log = "[GetDateText]: [This.GetName]: usa.11.o1 executed"
		add_political_power = -10
		add_stability = -0.001
	}
}

#2000 Phillips Explosion
country_event = {
	id = usa.12
	title = usa.12.t
	desc = usa.12.d
	picture = GFX_report_event_burning_oil_refinery
	
	is_triggered_only = yes
	
	trigger = {
		has_full_control_of_state = 800	#Texas
	}
	
	option = {
		name = usa.12.o1
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_political_power = -5
		add_stability = -0.01
		
		ai_chance = {
			factor = 0
		}
	}
	
	option = {
		name = usa.12.o2
		log = "[GetDateText]: [This.GetName]: usa.12.o2 executed"
		add_political_power = -25
		
		ai_chance = {
			factor = 100
		}
	}
}

#All befriended countries: USA stands with us!
country_event = {
	id = usa.13
	title = usa.13.t
	desc = usa.13.d
	picture = GFX_report_event_american_flag
	
	is_triggered_only = yes
	
	option = {
		name = usa.13.o1
		log = "[GetDateText]: [This.GetName]: usa.13.o1 executed"
		effect_tooltip = {
			add_opinion_modifier = {
				target = USA
				modifier = has_expressed_loyalty
			}
		}
	}
}

#All opposed countries: USA stands against us!
country_event = {
	id = usa.14
	title = usa.14.t
	desc = usa.14.d
	picture = GFX_report_event_american_flag
	
	is_triggered_only = yes
	
	option = {
		name = usa.14.o1
		log = "[GetDateText]: [This.GetName]: usa.14.o1 executed"
		effect_tooltip = {
			add_opinion_modifier = {
				target = USA
				modifier = loyal_to_our_enemy
			}
		}
	}
}

#Fort Worth Tornado
country_event = {
	id = usa.15
	title = usa.15.t
	desc = usa.15.d
	picture = GFX_report_event_tornado
	
	is_triggered_only = yes
	
	trigger = {
		has_full_control_of_state = 800	#Texas
	}
	
	option = {
		name = usa.15.o1
		log = "[GetDateText]: [This.GetName]: usa.15.o1 executed"
		add_stability = -0.01
	}
}

#Japan/SK/Taiwan: East Asian Military Support Program
country_event = {
	id = usa.16
	title = usa.16.t
	desc = usa.16.d
	picture = GFX_report_event_american_flag
	
	is_triggered_only = yes
	
	option = {
		name = usa.16.o1
		log = "[GetDateText]: [This.GetName]: usa.16.o1 executed"
		army_experience = 10
		navy_experience = 10
		air_experience = 10
	}
}

#The Sanders Craze
country_event = {
	id = usa.17
	title = usa.17.t
	desc = usa.17.d
	picture = GFX_report_event_bernie_sanders
	
	fire_only_once = yes
	
	trigger = {
		tag = USA
		date > 2016.1.1
		has_idea = idea_USA_political_establishment
	}
	
	mean_time_to_happen = {
		days = 10
	}
	
	option = {	#A new era for leftism in America?
		name = usa.17.o1
		log = "[GetDateText]: [This.GetName]: usa.17.o1 executed"
		add_to_variable = {
			var = Neutral_Green
			value = 0.01
		}
		add_to_variable = {
			var = socialism
			value = 0.01
		}
		add_to_variable = {
			var = liberalism
			value = 0.01
		}
		recalculate_party = yes
		custom_effect_tooltip = usa.17.o1_tt
	}
}

#Third Party Gains
country_event = {
	id = usa.18
	title = usa.18.t
	desc = usa.18.d
	picture = GFX_report_event_gary_johnson
	
	trigger = {
		tag = USA
		has_idea = idea_USA_political_establishment
		has_government = democratic
	}
	
	mean_time_to_happen = {
		days = 500
	}
	
	option = {	#A new alternative?
		name = usa.18.o1
		log = "[GetDateText]: [This.GetName]: usa.18.o1 executed"
		
		add_to_variable = {
			var = liberalism
			value = -0.02
		}
		add_to_variable = {
			var = conservatism
			value = -0.02
		}
		recalculate_party = yes
		custom_effect_tooltip = usa.18.o1_tt
	}
}

#Republican Nomination 2016
country_event = {
	id = usa.19
	title = usa.19.t
	desc = usa.19.d
	
	is_triggered_only = yes
	
	trigger = {
		USA = { has_elections = yes }
		USA = { has_idea = idea_USA_political_establishment }
	}
	
	option = {	#I dont care
		name = usa.19.o1
		log = "[GetDateText]: [This.GetName]: usa.19.o1 executed"
		random_list = {
			80 = { 
				set_country_flag = republican_nomination_2016_trump
				if = {
					limit = { check_variable = { var = conservatism value = 55 compare = less_than_or_equals } }
					create_country_leader = {
						name = "Donald Trump"
						picture = "Donald_Trump.dds"
						ideology = conservatism
					}
				}
			}
			15 = { 
				set_country_flag = republican_nomination_2016_cruz
				if = {
					limit = { check_variable = { var = conservatism value = 55 compare = less_than_or_equals } }
					create_country_leader = {
						name = "Ted Cruz"
						picture = "Ted_Cruz.dds"
						ideology = conservatism
					}
				}
			}
			4 = { 
				set_country_flag = republican_nomination_2016_rubio 
				if = {
					limit = { check_variable = { var = conservatism value = 55 compare = less_than_or_equals } }
					create_country_leader = {
						name = "Marco Rubio"
						picture = "Marco_Rubio.dds"
						ideology = conservatism
					}
				}
			}
			1 = { 
				set_country_flag = republican_nomination_2016_kasich
				if = {
					limit = { check_variable = { var = conservatism value = 55 compare = less_than_or_equals } }
					create_country_leader = {
						name = "John Kasich"
						picture = "John_Kasich.dds"
						ideology = conservatism
					}
				}
			}
		}
		ai_chance = {
			factor = 100
			modifier = {
				is_historical_focus_on = yes
				factor = 0
			}
		}
	}
	
	option = {	#Trump
		name = usa.19.o2
		log = "[GetDateText]: [This.GetName]: usa.19.o2 executed"
		set_country_flag = republican_nomination_2016_trump
		if = {
			limit = { check_variable = { var = conservatism value = 55 compare = less_than_or_equals } }
			create_country_leader = {
				name = "Donald Trump"
				picture = "Donald_Trump.dds"
				ideology = conservatism
			}
		}
		
		ai_chance = {
			factor = 100
			modifier = {
				is_historical_focus_on = no
				factor = 0
			}
		}
	}
	
	option = {	#Cruz
		name = usa.19.o3
		log = "[GetDateText]: [This.GetName]: usa.19.o3 executed"
		set_country_flag = republican_nomination_2016_cruz
		if = {
			limit = { check_variable = { var = conservatism value = 55 compare = less_than_or_equals } }
			create_country_leader = {
				name = "Ted Cruz"
				picture = "Ted_Cruz.dds"
				ideology = conservatism
			}
		}
		
		ai_chance = { factor = 0 }
	}
	
	option = {	#Rubio
		name = usa.19.o4
		log = "[GetDateText]: [This.GetName]: usa.19.o4 executed"
		set_country_flag = republican_nomination_2016_rubio
		if = {
			limit = { check_variable = { var = conservatism value = 55 compare = less_than_or_equals } }
			create_country_leader = {
				name = "Marco Rubio"
				picture = "Marco_Rubio.dds"
				ideology = conservatism
			}
		}
		
		ai_chance = { factor = 0 }
	}
	
	option = {	#Kasich
		name = usa.19.o5
		log = "[GetDateText]: [This.GetName]: usa.19.05 executed"
		set_country_flag = republican_nomination_2016_kasich
		if = {
			limit = { check_variable = { var = conservatism value = 55 compare = less_than_or_equals } }
			create_country_leader = {
				name = "John Kasich"
				picture = "John_Kasich.dds"
				ideology = conservatism
			}
		}
		
		ai_chance = { factor = 0 }
	}
}

#Democratic Nomination 2016
country_event = {
	id = usa.20
	title = usa.20.t
	desc = usa.20.d
	
	is_triggered_only = yes
	
	trigger = {
		USA = { has_elections = yes }
		USA = { has_idea = idea_USA_political_establishment }
	}
	
	option = {	#I dont care
		name = usa.20.o1
		log = "[GetDateText]: [This.GetName]: usa.20.o1 executed"
		random_list = {
			70 = { 
				set_country_flag = democratic_nomination_2016_clinton
				if = {
					limit = { check_variable = { var = liberalism value = 55 compare = less_than_or_equals } }
					create_country_leader = {
						name = "Hillary Clinton"
						picture = "Hillary_Clinton.dds"
						ideology = liberalism
					}
				}
			}
			30 = { 
				set_country_flag = democratic_nomination_2016_sanders 
				if = {
					limit = { check_variable = { var = liberalism value = 55 compare = less_than_or_equals } }
					create_country_leader = {
						name = "Bernie Sanders"
						picture = "Bernie_Sanders.dds"
						ideology = liberalism
					}
				}
			}
		}
		
		ai_chance = {
			factor = 100
			modifier = {
				is_historical_focus_on = yes
				factor = 0
			}
		}
	}
	
	option = {	#Clinton
		name = usa.20.o2
		log = "[GetDateText]: [This.GetName]: usa.20.o2 executed"
		set_country_flag = democratic_nomination_2016_clinton
		if = {
			limit = { check_variable = { var = liberalism value = 55 compare = less_than_or_equals } }
			create_country_leader = {
				name = "Hillary Clinton"
				picture = "Hillary_Clinton.dds"
				ideology = liberalism
			}
		}
		
		ai_chance = {
			factor = 100
			modifier = {
				is_historical_focus_on = no
				factor = 0
			}
		}
	}
	
	option = {	#Sanders
		name = usa.20.o3
		log = "[GetDateText]: [This.GetName]: usa.20.o3 executed"
		set_country_flag = democratic_nomination_2016_sanders
		if = {
			limit = { check_variable = { var = liberalism value = 55 compare = less_than_or_equals } }
			create_country_leader = {
				name = "Bernie Sanders"
				picture = "Bernie_Sanders.dds"
				ideology = liberalism
			}
		}
		
		ai_chance = { factor = 0 }
	}
}

#Totalitarians in our ranks trigger (other countries)
country_event = {
	id = usa.21
	hidden = yes
	
	trigger = {
		is_in_faction_with = USA
		has_elections = no
		NOT = { original_tag = USA }
		USA = { is_faction_leader = yes }
		USA = { has_elections = yes }
		USA = { NOT = { has_country_flag = tolerates_totalitarians_in_NATO } }
	}
	
	mean_time_to_happen = {
		days = 45
	}
	
	option = {
		log = "[GetDateText]: [This.GetName]: usa.21 executed"
		USA = { country_event = usa.22 }
	}
}

#Totalitarians in our ranks
country_event = {
	id = usa.22
	title = usa.22.t
	desc = usa.22.d
	picture = GFX_report_event_nato
	
	is_triggered_only = yes
	
	option = {	#They have to leave
		name = usa.22.o1
		log = "[GetDateText]: [This.GetName]: usa.22.o1 executed"
		remove_from_faction = FROM
		FROM = {
			country_event = diplomatic_message.2
		}
		FROM = {
			add_opinion_modifier = {
				modifier = kicked_from_faction
				target = ROOT
			}
		}
		
		ai_chance = {
			factor = 100
		}
	}
	
	option = {	#Uhhh, let's just look past that
		name = usa.22.o2
		log = "[GetDateText]: [This.GetName]: usa.22.o2 executed"
		add_stability = -0.05
		set_country_flag = tolerates_totalitarians_in_NATO
		news_event = usa.23
		FROM = {
			add_opinion_modifier = {
				target = USA
				modifier = recent_actions_very_positive
			}
		}
		every_country = {
			limit = {
				NOT = { original_tag = USA }
				is_in_faction_with = USA
				has_elections = yes
			}
			add_opinion_modifier = {
				target = USA
				modifier = tolerates_totalitarians_in_faction_faction_member
			}
		}
		every_country = {
			limit = {
				NOT = { original_tag = USA }
				NOT = { is_in_faction_with = USA }
				has_elections = yes
			}
			add_opinion_modifier = {
				target = USA
				modifier = tolerates_totalitarians_in_faction
			}
		}
		
		ai_chance = {
			factor = 0
			
			modifier = {
				add = 5
				is_historical_focus_on = no
			}
		}
	}
}

news_event = {	#Toleration of Totalitarians in NATO
	id = usa.23
	title = usa.23.t
	desc = usa.23.d
	picture = GFX_news_event_nato
	
	is_triggered_only = yes
	
	option = {
		name = usa.23.o1
		log = "[GetDateText]: [This.GetName]: usa.23.o1 executed"
		trigger = { original_tag = USA }
	}
	
	option = {
		name = usa.23.o2
		log = "[GetDateText]: [This.GetName]: usa.23.o2 executed"
		trigger = {
			NOT = { original_tag = USA }
			is_in_faction_with = USA
			has_elections = yes
		}
	}
	
	option = {
		name = usa.23.o3
		log = "[GetDateText]: [This.GetName]: usa.23.o3 executed"
		trigger = {
			NOT = { original_tag = USA }
			is_in_faction_with = USA
			has_elections = no
		}
	}
	
	option = {
		name = usa.23.o4
		log = "[GetDateText]: [This.GetName]: usa.23.o4 executed"
		trigger = {
			NOT = { original_tag = USA }
			NOT = { original_tag = SOV }
			NOT = { is_in_faction_with = USA }
			has_elections = yes
			NOT = { has_idea = neutrality_idea }
		}
	}
	
	option = {
		name = usa.23.o5
		log = "[GetDateText]: [This.GetName]: usa.23.o5 executed"
		trigger = {
			NOT = { original_tag = USA }
			NOT = { original_tag = SOV }
			NOT = { is_in_faction_with = USA }
			has_elections = yes
			has_idea = neutrality_idea
		}
	}
	
	option = {
		name = usa.23.o6
		log = "[GetDateText]: [This.GetName]: usa.23.o6 executed"
		trigger = {
			NOT = { original_tag = USA }
			original_tag = SOV
			NOT = { is_in_faction_with = USA }
			has_elections = yes
		}
	}
	
	option = {
		name = usa.23.o7
		log = "[GetDateText]: [This.GetName]: usa.23.o7 executed"
		trigger = {
			NOT = { original_tag = USA }
			NOT = { is_in_faction_with = USA }
			has_elections = no
		}
	}
}

country_event = {	#The New President lost the Popular Vote
	id = usa.25
	title = usa.25.t
	desc = usa.25.d
	picture = GFX_report_event_protests
	
	is_triggered_only = yes
	
	option = {
		name = usa.25.o1
		log = "[GetDateText]: [This.GetName]: usa.25.o1 executed"
		add_stability = -0.03
	}
}

country_event = {	#Choose our successor
	id = usa.26
	title = usa.26.t
	desc = usa.26.d
	picture = GFX_report_event_nato
	
	is_triggered_only = yes
	
	option = {	#CAN
		name = usa.26.o1
		log = "[GetDateText]: [This.GetName]: usa.26.o1 executed"
		trigger = {
			CAN = { num_of_factories > 49 }
			CAN = { has_elections = yes }
			CAN = { is_puppet = no }
			is_in_faction_with = CAN
		}
		every_country = {
			limit = {
				NOT = { tag = CAN }
				NOT = { tag = USA }
				is_in_faction_with = USA
			}
			set_country_flag = to_be_added_to_new_nato
		}
		CAN = { 
			create_faction = NATO
			country_event = usa.27
		}
		hidden_effect = {
			every_country = {
				limit = { 
					has_country_flag = to_be_added_to_new_nato
				}
				CAN = { add_to_faction = PREV }
				clr_country_flag = to_be_added_to_new_nato
				set_country_flag = seeks_canadian_alliance
			}
		}
		dismantle_faction = yes
	}
	
	option = {	#ENG
		name = usa.26.o2
		log = "[GetDateText]: [This.GetName]: usa.26.o2 executed"
		trigger = {
			ENG = { num_of_factories > 49 }
			ENG = { has_elections = yes }
			ENG = { is_puppet = no }
			is_in_faction_with = ENG
		}
		every_country = {
			limit = {
				NOT = { tag = ENG }
				NOT = { tag = USA }
				is_in_faction_with = USA
			}
			set_country_flag = to_be_added_to_new_nato
		}
		ENG = { 
			create_faction = NATO
			country_event = usa.27
		}
		hidden_effect = {
			every_country = {
				limit = { 
					has_country_flag = to_be_added_to_new_nato
				}
				ENG = { add_to_faction = PREV }
				clr_country_flag = to_be_added_to_new_nato
				set_country_flag = seeks_british_alliance
			}
		}
		dismantle_faction = yes
	}
	
	option = {	#FRA
		name = usa.26.o3
		log = "[GetDateText]: [This.GetName]: usa.26.o3 executed"
		trigger = {
			FRA = { num_of_factories > 49 }
			FRA = { has_elections = yes }
			FRA = { is_puppet = no }
			is_in_faction_with = FRA
		}
		every_country = {
			limit = {
				NOT = { tag = FRA }
				NOT = { tag = USA }
				is_in_faction_with = USA
			}
			set_country_flag = to_be_added_to_new_nato
		}
		FRA = { 
			create_faction = NATO
			country_event = usa.27
		}
		hidden_effect = {
			every_country = {
				limit = { 
					has_country_flag = to_be_added_to_new_nato
				}
				FRA = { add_to_faction = PREV }
				clr_country_flag = to_be_added_to_new_nato
				set_country_flag = seeks_french_alliance
			}
		}
		dismantle_faction = yes
	}
	
	option = {	#GER
		name = usa.26.o4
		log = "[GetDateText]: [This.GetName]: usa.26.o4 executed"
		trigger = {
			GER = { num_of_factories > 49 }
			GER = { has_elections = yes }
			GER = { is_puppet = no }
			is_in_faction_with = GER
		}
		every_country = {
			limit = {
				NOT = { tag = GER }
				NOT = { tag = USA }
				is_in_faction_with = USA
			}
			set_country_flag = to_be_added_to_new_nato
		}
		GER = { 
			create_faction = NATO
			country_event = usa.27
		}
		hidden_effect = {
			every_country = {
				limit = { 
					has_country_flag = to_be_added_to_new_nato
				}
				GER = { add_to_faction = PREV }
				clr_country_flag = to_be_added_to_new_nato
				set_country_flag = seeks_german_alliance
			}
		}
		dismantle_faction = yes
	}
	
	option = {	#ITA
		name = usa.26.o5
		log = "[GetDateText]: [This.GetName]: usa.26.o5 executed"
		trigger = {
			ITA = { num_of_factories > 49 }
			ITA = { has_elections = yes }
			is_in_faction_with = ITA
		}
		every_country = {
			limit = {
				NOT = { tag = ITA }
				NOT = { tag = USA }
				is_in_faction_with = USA
			}
			set_country_flag = to_be_added_to_new_nato
		}
		ITA = { 
			create_faction = NATO
			country_event = usa.27
		}
		hidden_effect = {
			every_country = {
				limit = { 
					has_country_flag = to_be_added_to_new_nato
				}
				ITA = { add_to_faction = PREV }
				clr_country_flag = to_be_added_to_new_nato
				set_country_flag = seeks_italian_alliance
			}
		}
		dismantle_faction = yes
	}
}

country_event = { #Focus Tree: USA makes us NATO leader!
	id = usa.27
	title = usa.27.t
	desc = usa.27.d
	picture = GFX_report_event_nato
	
	is_triggered_only = yes
	
	option = {	#Excellent news!
		name = usa.27.o1
		log = "[GetDateText]: [This.GetName]: usa.27.o1 executed"
		add_political_power = 300
	}	
}

country_event = { #Focus Tree: Invitation to the American Faction
	id = usa.28
	title = usa.28.t
	desc = usa.28.d
	picture = GFX_report_event_nato
	
	is_triggered_only = yes
	
	option = {	#We accept!
		name = usa.28.o1
		log = "[GetDateText]: [This.GetName]: usa.28.o1 executed"
		USA = { add_to_faction = ROOT }
		hidden_effect = {
			USA = { country_event = diplomatic_response.1 }
		}
		set_country_flag = seeks_american_alliance
		ai_chance = { 
			base = 100
			modifier = {
				is_faction_leader = yes
				add = -100
			}
		}
	}
	
	option = {	#We refuse!
		name = usa.28.o2
		log = "[GetDateText]: [This.GetName]: usa.28.o2 executed"
		add_political_power = 20
		hidden_effect = {
			USA = { country_event = diplomatic_response.2 }
		}
		ai_chance = { 
			base = 0
			modifier = {
				is_in_faction = yes 
				add = 50
			}
		}
	}
}

country_event = { #American Infrastructure Investments
	id = usa.29
	title = usa.29.t
	desc = usa.29.d
	picture = GFX_report_event_nato
	
	is_triggered_only = yes
	
	option = {	#Excellent.
		name = usa.29.o1
		log = "[GetDateText]: [This.GetName]: usa.29.o1 executed"
		add_political_power = -100
		random_owned_controlled_state = {
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		random_owned_controlled_state = {
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
	}
	
	option = {	#Use the funding for other purposes.
		name = usa.29.o2
		log = "[GetDateText]: [This.GetName]: usa.29.o2 executed"
		add_stability = 0.01
		add_political_power = 10
		add_stability = 0.01
	}
}

country_event = { #American Stabilization Effort
	id = usa.30
	title = usa.30.t
	desc = usa.30.d
	picture = GFX_report_event_nato
	
	is_triggered_only = yes
	
	option = {	#Excellent.
		name = usa.30.o1
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_stability = 0.03
	}
}

country_event = { #American Military Mission
	id = usa.31
	title = usa.31.t
	desc = usa.31.d
	picture = GFX_report_event_american_flag
	
	is_triggered_only = yes
	
	option = {	#Excellent.
		name = usa.31.o1
		log = "[GetDateText]: [This.GetName]: usa.31.o1 executed"
		add_stability = 0.02
		add_political_power = 100
		add_stability = 0.03
		army_experience = 50
		navy_experience = 50
		air_experience = 50
	}
}

#2000 U.S. Census
country_event = {
	id = usa.46
	title = usa.46.t
	desc = usa.46.d
	picture = GFX_report_event_election
	
	is_triggered_only = yes
	
	option = {
		name = usa.46.o1
		log = "[GetDateText]: [This.GetName]: usa.46.o1 executed"
		add_political_power = 10
		add_stability = 0.01
	}
}

#United States vs Microsoft Corp
country_event = {
	id = usa.47
	title = usa.47.t
	desc = usa.47.d
	picture = GFX_report_event_jury
	
	is_triggered_only = yes
	
	option = {
		name = usa.47.o1
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_political_power = 25
	}
	
	option = {
		name = usa.47.o2
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_stability = -0.02
		country_event = {
			id = usa.48
			days = 75
		}
	}
}

#Microsoft allowed to expand further
country_event = {
	id = usa.48
	title = usa.48.t
	desc = usa.48.d
	picture = GFX_report_event_jury
	
	is_triggered_only = yes
	
	option = {
		name = usa.48.o1
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_stability = -0.03
		if = {
			limit = {
				378 = {
					free_building_slots = { building = industrial_complex size > 1 include_locked = yes }
				}
			}
			378 = {
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = industrial_complex level = 1 instant_build = yes
				}
			}
		}
		if = {
			limit = {
				385 = {
					free_building_slots = { building = industrial_complex size > 1 include_locked = yes }
				}
			}
			385 = {
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = industrial_complex level = 1 instant_build = yes
				}
			}
		}
	}
}

#Marine Corps aircraft crash near Marana, Arizona
country_event = {
	id = usa.49
	title = usa.49.t
	desc = usa.49.d
	picture = GFX_report_event_plane_crash
	
	is_triggered_only = yes
	
	trigger = {
		has_full_control_of_state = 377	#Arizona
	}
	
	option = {
		name = usa.49.o1
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_political_power = -25
		navy_experience = 2
		air_experience = 2
	}
}

#Anti-Globalization Protests in Washington
country_event = {
	id = usa.50
	title = usa.50.t
	desc = usa.50.d
	picture = GFX_report_event_political_activist
	
	is_triggered_only = yes
	
	trigger = {
		has_full_control_of_state = 361	#Maryland/DC
		has_elections = yes
	}
	
	option = {	#The establishment is right.
		name = usa.50.o1
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_political_power = 10
		ai_chance = { factor = 100 }
	}
	
	option = {	#The environmentalists are right.
		name = usa.50.o2
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_political_power = -10
		ai_chance = { factor = 0 }
	}
	
	option = {	#Perhaps we should listen to the nationalists?
		name = usa.50.o3
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_political_power = -10
		ai_chance = { factor = 0 }
	}
}

#Elian Gonzalez seized by Federal Agents
country_event = {
	id = usa.51
	title = usa.51.t
	desc = usa.51.d
	picture = GFX_report_event_elian_gonzalez
	
	is_triggered_only = yes
	
	trigger = {
		has_government = democratic
	}
	
	option = {	#The establishment is right.
		name = usa.51.o1
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_to_variable = {
			var = liberalism
			value = 0.005
		}
		recalculate_party = yes
		custom_effect_tooltip = usa.51_tt
	}
}

#Shepherdstown Peace Talks
country_event = {
	id = usa.52
	title = usa.52.t
	desc = usa.52.d
	picture = GFX_report_event_political_deal
	
	is_triggered_only = yes
	
	option = {
		name = usa.52.o1
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_political_power = 5
		SYR = { country_event = usa.53 }
		ISR = { country_event = usa.54 }
	}
}

#Syria: Shepherdstown Peace Talks
country_event = {
	id = usa.53
	title = usa.53.t
	desc = usa.53.d
	picture = GFX_report_event_political_deal
	
	is_triggered_only = yes
	
	option = {
		name = usa.53.o1
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_opinion_modifier = {
			target = ISR
			modifier = peace_talks
		}
	}
}

#Israel: Shepherdstown Peace Talks
country_event = {
	id = usa.54
	title = usa.54.t
	desc = usa.54.d
	picture = GFX_report_event_political_deal
	
	is_triggered_only = yes
	
	option = {
		name = usa.54.o1
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_opinion_modifier = {
			target = SYR
			modifier = peace_talks
		}
	}
}

#Datapoint files for Chapter 11 bankruptcy
country_event = {
	id = usa.55
	title = usa.55.t
	desc = usa.55.d
	picture = GFX_report_event_computer
	
	is_triggered_only = yes
	
	option = {
		name = usa.55.o1
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_political_power = -10
	}
	
	option = {
		name = usa.55.o2
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		trigger = { has_elections = no }
		add_political_power = -100
		add_stability = 0.01
	}
}

#Republican National Convention 2000
country_event = {
	id = usa.56
	title = usa.56.t
	desc = usa.56.d
	picture = GFX_report_event_republican_national_convention
	
	is_triggered_only = yes
	
	trigger = {
		has_elections = yes
	}
	
	option = {
		name = usa.56.o1
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_popularity = {
			ideology = democratic
			popularity = 0.03
		}
	}
}

#DeviantART launched
country_event = {
	id = usa.57
	title = usa.57.t
	desc = usa.57.d
	picture = GFX_report_event_computer
	
	is_triggered_only = yes
	
	trigger = {
		has_elections = yes
	}
	
	option = {
		name = usa.57.o1
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_political_power = 3
	}
}

#H.L. Hunley
country_event = {
	id = usa.58
	title = usa.58.t
	desc = usa.58.d
	picture = GFX_report_event_submarine
	
	is_triggered_only = yes
	
	option = {
		name = usa.58.o1
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_political_power = 10
	}
}

#Democratic National Convention 2000
country_event = {
	id = usa.59
	title = usa.59.t
	desc = usa.59.d
	picture = GFX_report_event_democratic_national_convention
	
	is_triggered_only = yes
	
	trigger = {
		has_elections = yes
	}
	
	option = {
		name = usa.59.o1
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_to_variable = {
			var = liberalism
			value = 0.03
		}
		recalculate_party = yes
		custom_effect_tooltip = usa.59_tt
	}
}

#Mac OS X
country_event = {
	id = usa.60
	title = usa.60.t
	desc = usa.60.d
	picture = GFX_report_event_computer
	
	is_triggered_only = yes
	
	option = {
		name = usa.60.o1
		log = "[GetDateText]: [This.GetName]: usa.60.o1 executed"
		add_political_power = 3
	}
}

#Martin County Oil Spill
country_event = {
	id = usa.61
	title = usa.61.t
	desc = usa.61.d
	picture = GFX_report_event_burning_oil_refinery
	
	trigger = {
		has_full_control_of_state = 782	#Kentucky
	}
	
	is_triggered_only = yes
	
	option = {
		name = usa.61.o1
		log = "[GetDateText]: [This.GetName]: usa.61.o1 executed"
		add_political_power = -20
	}
	
	option = {
		name = usa.61.o2
		log = "[GetDateText]: [This.GetName]: usa.61.o2 executed"
		add_stability = -0.01
	}
}

#Gus Hall dies
country_event = {
	id = usa.62
	title = usa.62.t
	desc = usa.62.d
	picture = GFX_report_event_fathers_of_communism
	
	is_triggered_only = yes
	
	immediate = {
		set_country_flag = death_of_gus_hall
	}
	
	option = {
		name = usa.62.o1
		log = "[GetDateText]: [This.GetName]: usa.62.o1 executed"
		ai_chance = { factor = 100 }
	}
	
	option = {
		name = usa.62.o2
		log = "[GetDateText]: [This.GetName]: usa.62.o2 executed"
		add_political_power = -50
		add_stability = -0.02
		add_to_variable = {
			var = Communist-State
			value = 0.01
		}
		add_to_variable = {
			var = anarchist_communism
			value = 0.03
		}
		recalculate_party = yes
		custom_effect_tooltip = usa.62.o2_tt
		
		hidden_effect = { country_event = { id = usa.63 days = 1000 } }
		
		ai_chance = { factor = 0 }
	}
}

#Hall State
country_event = {
	id = usa.63
	title = usa.63.t
	desc = usa.63.d
	picture = GFX_report_event_communism
	
	is_triggered_only = yes
	
	trigger = {
		has_government = communism
	}
	
	option = {
		name = usa.63.o1
		log = "[GetDateText]: [This.GetName]: usa.63.o1 executed"
		add_political_power = 20
		#State Names
		809 = { set_state_name = "Halberg" }		#Washington			#Arvo Kustaa Halberg (Gus Hall)
		790 = { set_state_name = "Vladimir" }		#North Carolina		#Vladimir Lenin
		791 = { set_state_name = "Marx" }			#South Carolina		#Karl Marx
		797 = { set_state_name = "Delescluze" }		#Louisiana			#Louis Charles Delescluze
		773 = { set_state_name = "Josephia" }		#Virginia			#Joseph Stalin
		774 = { set_state_name = "Zedong" }			#West Virginia		#Mao Zedong
		792 = { set_state_name = "Malcolm" }		#Georgia			#Malcolm X
		803 = { set_state_name = "Manabendra" }		#New Mexico			#Manabendra Nath Roy
		765 = { set_state_name = "Inkpin" }			#New Hampshire		#Albert Inkpin
		770 = { set_state_name = "Browder" }		#New Jersey			#Earl Browder
		772 = { set_state_name = "Margot" }			#Maryland			#Margot Honecker
		771 = { set_state_name = "Eugene" }			#Pennsylvania		#Eugene Debs
		769 = { set_state_name = "Engels" }			#New York			#Friedrich Engels
		#Province Names
		set_province_name = { id = 3957 name = "Gus Hall City, D.M." }
		set_country_flag = washington_dc_renamed_due_to_gus_hall
		ai_chance = { factor = 100 }
	}
	
	option = {
		name = usa.63.o2
		log = "[GetDateText]: [This.GetName]: usa.63.o2 executed"
		ai_chance = { factor = 0 }
	}
}

#Madeleine Albright and Kim Jong-Il
country_event = {
	id = usa.64
	title = usa.64.t
	desc = usa.64.d
	picture = GFX_report_event_political_deal
	
	is_triggered_only = yes
	
	trigger = {
		has_government = democratic
	}
	
	option = {
		name = usa.64.o1
		log = "[GetDateText]: [This.GetName]: usa.12.o1 executed"
		add_opinion_modifier = { target = NKO modifier = new_diplomatic_missions }
		reverse_add_opinion_modifier = { target = NKO modifier = new_diplomatic_missions }
	}
}

#Hillary Clinton elected to the U.S. Senate
country_event = {
	id = usa.65
	title = usa.65.t
	desc = usa.65.d
	picture = GFX_report_event_hillary_clinton_young
	
	is_triggered_only = yes
	
	option = {
		name = usa.65.o1
		log = "[GetDateText]: [This.GetName]: usa.65.o1 executed"
		add_to_variable = {
			var = liberalism
			value = 0.001
		}
		recalculate_party = yes
		custom_effect_tooltip =
	}
}

#Bush v Gore: Florida Recount Stopped
country_event = {
	id = usa.66
	title = usa.66.t
	desc = usa.66.d
	picture = GFX_report_event_jury
	
	is_triggered_only = yes
	
	trigger = {
		has_government = democratic
	}
	
	option = {
		name = usa.66.o1
		log = "[GetDateText]: [This.GetName]: usa.66.o1 executed"
		add_stability = -0.01
	}
}

#December 2000 nor'easter
country_event = {
	id = usa.67
	title = usa.67.t
	desc = usa.67.d
	picture = GFX_report_event_blizzard
	
	is_triggered_only = yes
	
	option = {
		name = usa.67.o1
		log = "[GetDateText]: [This.GetName]: usa.67.o1 executed"
		add_political_power = -50
	}
}

#The Launch of Wikipedia
country_event = {
	id = usa.68
	title = usa.68.t
	desc = usa.68.d
	picture = GFX_report_event_computer
	
	trigger = {
		has_elections = yes
	}
	
	is_triggered_only = yes
	
	option = {
		name = usa.68.o1
		log = "[GetDateText]: [This.GetName]: usa.68.o1 executed"
		add_political_power = 10
	}
}

#Colin Powell attacks Zimbabwe
country_event = {
	id = usa.69
	title = usa.69.t
	desc = usa.69.d
	picture = GFX_report_event_united_states_army
	
	is_triggered_only = yes
	
	trigger = {
		ZIM = { has_elections = no }
	}
	
	option = {
		name = usa.69.o1
		log = "[GetDateText]: [This.GetName]: usa.69.o1 executed"
		add_political_power = -15
		ZIM = {
			add_opinion_modifier = {
				target = USA
				modifier = diplomatic_insults
			}
		}
		trigger = { NOT = { has_government = fascism } }
		ai_chance = { factor = 100 }
	}
	
	option = {
		name = usa.69.o2
		log = "[GetDateText]: [This.GetName]: usa.69.o2 executed"
		remove_unit_leader = 1
		ai_chance = { factor = 0 }
	}
}

#Hall State Reversal Event
country_event = {
	id = usa.70
	fire_only_once = yes
	hidden = yes
	
	trigger = {
		NOT = { has_government = communism }
		has_country_flag = washington_dc_renamed_due_to_gus_hall
	}
	
	option = {
		log = "[GetDateText]: [This.GetName]: usa.70 executed"
		clr_country_flag = washington_dc_renamed_due_to_gus_hall
		809 = { set_state_name = "Washington" }		#Washington
		790 = { set_state_name = "North Carolina" }	#North Carolina
		791 = { set_state_name = "South Carolina" }	#South Carolina
		797 = { set_state_name = "Louisiana" }		#Louisiana
		773 = { set_state_name = "Virginia" }		#Virginia
		774 = { set_state_name = "West Virginia" }	#West Virginia
		792 = { set_state_name = "Georgia" }		#Georgia
		803 = { set_state_name = "New Mexico" }		#New Mexico
		765 = { set_state_name = "New Hampshire" }	#New Hampshire
		770 = { set_state_name = "New Jersey" }		#New Jersey
		772 = { set_state_name = "Maryland" }		#Maryland
		771 = { set_state_name = "Pennsylvania" }	#Pennsylvania
		769 = { set_state_name = "New York" }		#New York
		set_province_name = { id = 3957 name = "Washington, D.C." }
	}
}