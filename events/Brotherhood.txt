﻿add_namespace = brotherhood

	country_event = {	#Muslim Brotherhood
	id = brotherhood.0
	
	hidden = yes
	
	trigger = {
		Is_Muslim_Brotherhood = yes
	}
	
	mean_time_to_happen = {
		days = 70
	}
	
	immediate = {
		log = "[GetDateText]: [This.GetName]: brotherhood.0.a immediate effect executed"
		remove_ideas = muslim_brotherhood_crackdown
	}
	
	option = {
		name = brotherhood.0.a
		log = "[GetDateText]: [This.GetName]: brotherhood.0.a option executed"
		if = {
		  limit = {
			 JOR = { Is_Muslim_Brotherhood = yes }
		  }
		  JOR = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 TUR = { Is_Muslim_Brotherhood = yes }
		  }
		  TUR = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 SYR = { Is_Muslim_Brotherhood = yes }
		  }
		  SYR = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 FSA = { Is_Muslim_Brotherhood = yes }
		  }
		  FSA = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 NUS = { Is_Muslim_Brotherhood = yes }
		  }
		  NUS = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 AFG = { Is_Muslim_Brotherhood = yes }
		  }
		  AFG = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 TAL = { Is_Muslim_Brotherhood = yes }
		  }
		  TAL = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 HAM = { Is_Muslim_Brotherhood = yes }
		  }
		  HAM = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 SAU = { Is_Muslim_Brotherhood = yes }
		  }
		  SAU = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 BHR = { Is_Muslim_Brotherhood = yes }
		  }
		  BHR = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 UAE = { Is_Muslim_Brotherhood = yes }
		  }
		  UAE = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 YEM = { Is_Muslim_Brotherhood = yes }
		  }
		  YEM = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 EGY = { Is_Muslim_Brotherhood = yes }
		  }
		  EGY = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 SUD = { Is_Muslim_Brotherhood = yes }
		  }
		  SUD = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 LBA = { Is_Muslim_Brotherhood = yes }
		  }
		  LBA = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 HOR = { Is_Muslim_Brotherhood = yes }
		  }
		  HOR = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 GNC = { Is_Muslim_Brotherhood = yes }
		  }
		  GNC = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 GNA = { Is_Muslim_Brotherhood = yes }
		  }
		  GNA = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 ALG = { Is_Muslim_Brotherhood = yes }
		  }
		  ALG = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 MOR = { Is_Muslim_Brotherhood = yes }
		  }
		  MOR = { country_event = { id = brotherhood.1 } }
		}
		if = {
		  limit = {
			 QAT = { Is_Muslim_Brotherhood = yes }
		  }
		  QAT = { country_event = { id = brotherhood.1 } }
		}
	}
}
country_event = {	# APPLY
	id = brotherhood.1
	
	hidden = yes
	
	is_triggered_only = yes
	
	option = {
		name = brotherhood.1.a
		log = "[GetDateText]: [This.GetName]: brotherhood.1.a option executed"
		PREV = { add_opinion_modifier = { target = FROM modifier = Muslim_Brotherhood_Relations } }
		#PREV = { reverse_add_opinion_modifier = { target = FROM modifier = Muslim_Brotherhood_Relations } }
	}
}

country_event = {	#Has crackdown
	id = brotherhood.2
	
	hidden = yes
	
	
	trigger = {
			has_idea = muslim_brotherhood_crackdown
	}
	
	mean_time_to_happen = {
		days = 21
	}
	
	option = {
		name = brotherhood.2.a
		log = "[GetDateText]: [This.GetName]: brotherhood.2.a option executed"
		if = {
		  limit = {
			 JOR = { Is_Muslim_Brotherhood = yes }
		  }
		  JOR = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 TUR = { Is_Muslim_Brotherhood = yes }
		  }
		  TUR = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 SYR = { Is_Muslim_Brotherhood = yes }
		  }
		  SYR = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 FSA = { Is_Muslim_Brotherhood = yes }
		  }
		  FSA = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 NUS = { Is_Muslim_Brotherhood = yes }
		  }
		  NUS = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 AFG = { Is_Muslim_Brotherhood = yes }
		  }
		  AFG = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 TAL = { Is_Muslim_Brotherhood = yes }
		  }
		  TAL = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 HAM = { Is_Muslim_Brotherhood = yes }
		  }
		  HAM = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 SAU = { Is_Muslim_Brotherhood = yes }
		  }
		  SAU = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 BHR = { Is_Muslim_Brotherhood = yes }
		  }
		  BHR = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 UAE = { Is_Muslim_Brotherhood = yes }
		  }
		  UAE = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 YEM = { Is_Muslim_Brotherhood = yes }
		  }
		  YEM = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 EGY = { Is_Muslim_Brotherhood = yes }
		  }
		  EGY = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 SUD = { Is_Muslim_Brotherhood = yes }
		  }
		  SUD = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 LBA = { Is_Muslim_Brotherhood = yes }
		  }
		  LBA = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 HOR = { Is_Muslim_Brotherhood = yes }
		  }
		  HOR = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 GNA = { Is_Muslim_Brotherhood = yes }
		  }
		  GNA = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 GNC = { Is_Muslim_Brotherhood = yes }
		  }
		  GNC = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 ALG = { Is_Muslim_Brotherhood = yes }
		  }
		  ALG = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 MOR = { Is_Muslim_Brotherhood = yes }
		  }
		  MOR = { country_event = { id = brotherhood.3 } }
		}
		if = {
		  limit = {
			 QAT = { Is_Muslim_Brotherhood = yes }
		  }
		  QAT = { country_event = { id = brotherhood.3 } }
		}
	}
}

country_event = {	# APPLY
	id = brotherhood.3
	
	hidden = yes
	
	is_triggered_only = yes
	
	option = {
		name = brotherhood.3.a
		log = "[GetDateText]: [This.GetName]: brotherhood.3.a option executed"
		PREV = { add_opinion_modifier = { target = FROM modifier = Is_cracking_down_on_Muslim_Brotherhood } }
		PREV = { reverse_add_opinion_modifier = { target = FROM modifier = Is_Muslim_Brotherhood_negative } }
	}
}

################ Muslim Brotherhood path to civil war ###########

#####Muslim Brotherhood demands elections / set crackdown
country_event = {
	id = brotherhood.6
	title = brotherhood.6.t
	desc = brotherhood.6.d
	
	trigger = {
		Is_Possible_Muslim_Brotherhood = yes
		NOT = { has_government = neutrality }
		NOT = { Is_Muslim_Brotherhood = yes }
		NOT = { has_elections = yes }
		NOT = { has_idea = muslim_brotherhood_crackdown }
		neutrality > 0.35
	}
	
	mean_time_to_happen = {
		days = 200
	}

	option = {
		name = brotherhood.6.a
		log = "[GetDateText]: [This.GetName]: brotherhood.6.a option executed"
		custom_effect_tooltip = muslim_brotherhood_crackdown_tt
		hidden_effect = { add_ideas = muslim_brotherhood_crackdown }
		if = {
		  limit = {
			has_idea = al_jazeera_allowed
		  }
		  swap_ideas = { remove_idea = al_jazeera_allowed add_idea = al_jazeera_banned }
		}
	}
	option = {
		name = brotherhood.6.b
		log = "[GetDateText]: [This.GetName]: brotherhood.6.b option executed"
		if = {
			limit = {
				has_government = democratic
			}
			set_politics = {
				ruling_party = democratic
				elections_allowed = yes
			}
		}
		if = {
			limit = {
				has_government = communism
			}
			set_politics = {
				ruling_party = communism
				elections_allowed = yes
			}
		}
		if = {
			limit = {
				has_government = fascism
			}
			set_politics = {
				ruling_party = fascism
				elections_allowed = yes
			}
		}
		if = {
			limit = {
				has_government = neutrality
			}
			set_politics = {
				ruling_party = neutrality
				elections_allowed = yes
			}
		}
		if = {
			limit = {
				has_government = nationalist
			}
			set_politics = {
				ruling_party = nationalist
				elections_allowed = yes
			}
		}
		if = {
			limit = {
				has_idea = al_jazeera_banned
			}
		  swap_ideas = { remove_idea = al_jazeera_banned add_idea = al_jazeera_allowed }
		}
		if = {
			limit = {
				has_idea = muslim_brotherhood_crackdown
			}
		  remove_ideas = muslim_brotherhood_crackdown
		}
	}
}

#####Muslim Brotherhood crackdown backfires
country_event = {
	id = brotherhood.7
	title = brotherhood.7.t
	desc = brotherhood.7.d
	
	trigger = {
		has_idea = muslim_brotherhood_crackdown
		Is_Possible_Muslim_Brotherhood = yes
		NOT = { has_government = neutrality }
		NOT = { Is_Muslim_Brotherhood = yes }
		NOT = { has_elections = yes }
		neutrality > 0.2
		}

		mean_time_to_happen = {
			days = 1000
			#Economic factors
			modifier = {
				factor = 1.4
				has_idea = economic_boom
			}
			modifier = {
				factor = 1.2
				has_idea = fast_growth
			}
			modifier = {
				factor = 0.9
				has_idea = stagnation
			}
			modifier = {
				factor = 0.8
				has_idea = recession
			}
			modifier = {
				factor = 0.7
				has_idea = depression
			}
			modifier = {
				factor = 0.9
				has_idea = gdp_4
			}
			modifier = {
				factor = 0.9
				has_idea = gdp_3
			}
			modifier = {
				factor = 0.8
				has_idea = gdp_2
			}
			modifier = {
				factor = 0.7
				has_idea = gdp_1
			}
			modifier = {
				factor = 0.5
				has_idea = paralyzing_corruption
			}
			modifier = {
				factor = 0.5
				has_idea = crippling_corruption
			}
			modifier = {
				factor = 0.6
				has_idea = rampant_corruption
			}
			modifier = {
				factor = 0.7
				has_idea = unrestrained_corruption
			}
			modifier = {
				factor = 0.8
				has_idea = systematic_corruption
			}
			modifier = {
				factor = 0.9
				has_idea = widespread_corruption
			}
			
			#Policy factors
			modifier = {
				factor = 1.5
				has_idea = police_05
			}
			modifier = {
				factor = 1.3
				has_idea = police_04
			}
			modifier = {
				factor = 0.9
				has_idea = police_02
			}
			modifier = {
				factor = 0.7
				has_idea = police_01
			}
			modifier = {
				factor = 1.2
				has_idea = bureau_05
			}
			modifier = {
				factor = 1.1
				has_idea = bureau_04
			}
			modifier = {
				factor = 0.9
				has_idea = bureau_02
			}
			modifier = {
				factor = 0.8
				has_idea = bureau_01
			}
			modifier = {
				factor = 1.6
				has_idea = social_06
			}
			modifier = {
				factor = 1.4
				has_idea = social_05
			}
			modifier = {
				factor = 1.2
				has_idea = social_04
			}
		}

	option = {
		name = brotherhood.7.a
		log = "[GetDateText]: [This.GetName]: brotherhood.7.a option executed"
		add_popularity = {
		ideology = neutrality
		popularity = 0.05
		}
	}
}

country_event = {
	id = brotherhood.8
	title = brotherhood.8.t
	desc = brotherhood.8.d
	
	trigger = {
		has_idea = muslim_brotherhood_crackdown
		Is_Possible_Muslim_Brotherhood = yes
		NOT = { has_government = neutrality }
		NOT = { Is_Muslim_Brotherhood = yes }
		NOT = { has_elections = yes }
		neutrality > 0.2
		}

		mean_time_to_happen = {
			days = 1000
			#Economic factors
			modifier = {
				factor = 1.4
				has_idea = economic_boom
			}
			modifier = {
				factor = 1.2
				has_idea = fast_growth
			}
			modifier = {
				factor = 0.9
				has_idea = stagnation
			}
			modifier = {
				factor = 0.8
				has_idea = recession
			}
			modifier = {
				factor = 0.7
				has_idea = depression
			}
			modifier = {
				factor = 0.9
				has_idea = gdp_4
			}
			modifier = {
				factor = 0.9
				has_idea = gdp_3
			}
			modifier = {
				factor = 0.8
				has_idea = gdp_2
			}
			modifier = {
				factor = 0.7
				has_idea = gdp_1
			}
			modifier = {
				factor = 0.5
				has_idea = paralyzing_corruption
			}
			modifier = {
				factor = 0.5
				has_idea = crippling_corruption
			}
			modifier = {
				factor = 0.6
				has_idea = rampant_corruption
			}
			modifier = {
				factor = 0.7
				has_idea = unrestrained_corruption
			}
			modifier = {
				factor = 0.8
				has_idea = systematic_corruption
			}
			modifier = {
				factor = 0.9
				has_idea = widespread_corruption
			}
			
			#Policy factors
			modifier = {
				factor = 1.5
				has_idea = police_05
			}
			modifier = {
				factor = 1.3
				has_idea = police_04
			}
			modifier = {
				factor = 0.9
				has_idea = police_02
			}
			modifier = {
				factor = 0.7
				has_idea = police_01
			}
			modifier = {
				factor = 1.2
				has_idea = bureau_05
			}
			modifier = {
				factor = 1.1
				has_idea = bureau_04
			}
			modifier = {
				factor = 0.9
				has_idea = bureau_02
			}
			modifier = {
				factor = 0.8
				has_idea = bureau_01
			}
			modifier = {
				factor = 1.6
				has_idea = social_06
			}
			modifier = {
				factor = 1.4
				has_idea = social_05
			}
			modifier = {
				factor = 1.2
				has_idea = social_04
			}
		}

	option = {
		name = brotherhood.8.a
		log = "[GetDateText]: [This.GetName]: brotherhood.8.a option executed"
		add_popularity = {
		ideology = neutrality
		popularity = 0.05
		}
	}
}

country_event = {
	id = brotherhood.9
	title = brotherhood.9.t
	desc = brotherhood.9.d
	
	trigger = {
		has_idea = muslim_brotherhood_crackdown
		Is_Possible_Muslim_Brotherhood = yes
		NOT = { has_government = neutrality }
		NOT = { Is_Muslim_Brotherhood = yes }
		NOT = { has_elections = yes }
		neutrality > 0.2
	}

		mean_time_to_happen = {
			days = 1000
			#Economic factors
			modifier = {
				factor = 1.1
				has_idea = gdp_4
			}
			modifier = {
				factor = 1.2
				has_idea = gdp_3
			}
			modifier = {
				factor = 1.3
				has_idea = gdp_2
			}
			modifier = {
				factor = 1.4
				has_idea = gdp_1
			}
			modifier = {
				factor = 1.5
				has_idea = paralyzing_corruption
			}
			modifier = {
				factor = 1.4
				has_idea = crippling_corruption
			}
			modifier = {
				factor = 1.3
				has_idea = rampant_corruption
			}
			modifier = {
				factor = 1.2
				has_idea = unrestrained_corruption
			}
			modifier = {
				factor = 1.1
				has_idea = systematic_corruption
			}
			
			#Policy factors
			modifier = {
				factor = 0.4
				has_idea = police_05
			}
			modifier = {
				factor = 0.6
				has_idea = police_04
			}
			modifier = {
				factor = 0.8
				has_idea = police_03
			}
			modifier = {
				factor = 1.1
				has_idea = police_02
			}
			modifier = {
				factor = 1.3
				has_idea = police_01
			}
			modifier = {
				factor = 0.7
				has_idea = bureau_05
			}
			modifier = {
				factor = 0.8
				has_idea = bureau_04
			}
			modifier = {
				factor = 1.1
				has_idea = bureau_02
			}
			modifier = {
				factor = 1.2
				has_idea = bureau_01
			}
			modifier = {
				factor = 0.5
				has_war = yes
			}
		}

	option = {
		name = brotherhood.9.a
		log = "[GetDateText]: [This.GetName]: brotherhood.9.a option executed"
		add_popularity = {
		ideology = neutrality
		popularity = -0.1
		}
	}
}

country_event = {
	id = brotherhood.10
	title = brotherhood.10.t
	desc = brotherhood.10.d
	
	trigger = {
		has_idea = muslim_brotherhood_crackdown
		NOT = { has_war = yes }
		Is_Possible_Muslim_Brotherhood = yes
		NOT = { has_government = neutrality }
		NOT = { Is_Muslim_Brotherhood = yes }
		NOT = { has_elections = yes }
		neutrality > 0.5
	}

	mean_time_to_happen = {
		days = 150
	}

	option = {
		name = brotherhood.10.a
		log = "[GetDateText]: [This.GetName]: brotherhood.10.a executed"
		start_civil_war = { ideology = neutrality size = 0.5 }
		add_popularity = {
			ideology = neutrality
			popularity = -0.3
		}
	}
	option = { #play as the brotherhood
		name = brotherhood.10.b
		log = "[GetDateText]: [This.GetName]: brotherhood.10.b executed"
		trigger = { is_ai = no }
		if = {
		limit = { has_government = democratic }
		start_civil_war = {
				ruling_party = neutrality
				ideology = democratic
				size = 0.6
			}
		}
		if = {
		limit = { has_government = communism }
		start_civil_war = {
				ruling_party = neutrality
				ideology = communism
				size = 0.6
			}
		}
		if = {
		limit = { has_government = fascism }
		start_civil_war = {
				ruling_party = neutrality
				ideology = fascism
				size = 0.6
			}
		}
		if = {
		limit = { has_government = nationalist }
		start_civil_war = {
				ruling_party = neutrality
				ideology = nationalist
				size = 0.6
			}
		}
	}
}