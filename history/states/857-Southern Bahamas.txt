state = {
	id = 857
	name = "STATE_857"
	manpower = 8362
	state_category = state_00

	history = {
		owner = BAH
		victory_points = { 1513 1 } #George Town
		
		buildings = {
			infrastructure = 5
			
			1513 = {
				naval_base = 1
			}

		}
		add_core_of = BAH
		2017.1.1 = {
			add_manpower = 2905
		}
	}

	provinces = {
		  1513
		
		}
}
