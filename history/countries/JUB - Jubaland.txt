﻿2000.1.1 = {
	capital = 581
	oob = "JUB_2000"
	set_convoys = 20
	
	add_ideas = {
		#pop_030
		defence_09
	}
	
	#Influence system
	init_influence = yes
	set_variable = { domestic_influence_amount = 30 }
	add_to_array = { influence_array = KEN.id }
	add_to_array = { influence_array_val = 20 }
	add_to_array = { influence_array = SOM.id }
	add_to_array = { influence_array_val = 5 }
	add_to_array = { influence_array = ETH.id }
	add_to_array = { influence_array_val = 2 }
	add_to_array = { influence_array = USA.id }
	add_to_array = { influence_array_val = 3 }
	startup_influence = yes
	
	complete_national_focus = bonus_tech_slots
	
	# Starting tech
	set_technology = { 
		legacy_doctrines = 1 
		modern_blitzkrieg = 1 
		forward_defense = 1 
		encourage_nco_iniative = 1 
		air_land_battle = 1
		infantry_weapons = 1
		
		command_control_equipment = 1
		
		Anti_tank_0 = 1
		Anti_Air_0 = 1
		
	}
	
	set_politics = {

		parties = {
			democratic = { 
				popularity = 10
			}

			fascism = {
				popularity = 10
			}
			
			communism = {
				popularity = 10
				#banned = no #default is no
			}
			
			neutrality = { 
				popularity = 20
			}
			
			nationalist = { 
				popularity = 50
			}
		}
		
		ruling_party = nationalist
		last_election = "1999.11.10"
		election_frequency = 48
		elections_allowed = no
	}

	create_country_leader = {
		name = "Mohammed Said Hersi Morgan"
		picture = "JUB_Mohammed_Said_Hersi_Morgan.dds"
		ideology = Nat_Autocracy
		traits = {
			nationalist_Nat_Autocracy
		}
	}
	
}